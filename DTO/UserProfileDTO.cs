﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AITeamAPI.DTO
{
    public class UserProfileDTO
    {
        public string Name { get; set; }
        public string EmailId { get; set; }
        public string Pincode { get; set; }
        public string VerifierTeamId { get; set; }
        public DateTime DOB { get; set; }
        public string ProfileImage { get; set; }

    }
}
