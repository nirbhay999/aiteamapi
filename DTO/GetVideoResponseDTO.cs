﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AITeamAPI.DTO
{
    public class GetVideoResponseDTO
    {
        public string Username { get; set; }
        public string VideoId { get; set; }
    }
}
