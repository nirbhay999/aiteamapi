﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AITeamAPI.DTO
{
    public class SetPassword
    {
        public string MobileNumber { get; set; }
        public string Password { get; set; }
    }
}
