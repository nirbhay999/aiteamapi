﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AITeamAPI.DTO
{
    public class LoginDTO
    {
        public string MobileNumber { get; set; }
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}
