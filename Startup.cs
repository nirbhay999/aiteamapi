﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AITeamAPI.BusinessLogic;
using AITeamAPI.Entities;
using AITeamAPI.Repository;
using AITeamAPI.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AITeamAPI
{
    public class Startup
    {
        private IConfiguration _config;
        public Startup(IConfiguration configuration)
        {
            _config = configuration;
        }

        //public IConfiguration _conf { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IMessageSender, MessageSender>();
            services.AddDbContext<MyDbContext>(options =>
            {
                options.UseMySql(_config["Data:MySql"]);
            }, ServiceLifetime.Scoped);
            services.AddIdentity<AppUser, IdentityRole>(config =>
            {
                // Password settings
                config.Password.RequireUppercase = false;
                config.Password.RequireDigit = true;
                config.Password.RequireNonAlphanumeric = true;

                //Lockout settings
                config.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromHours(1);
                config.Lockout.MaxFailedAccessAttempts = 6;
                //Phoen number settings
                config.SignIn.RequireConfirmedPhoneNumber = true;

            }).AddEntityFrameworkStores<MyDbContext>().AddDefaultTokenProviders();
            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(5);

                options.LoginPath = "/Identity/Account/Login";
                options.AccessDeniedPath = "/Identity/Account/AccessDenied";
                options.SlidingExpiration = true;
            });

            services.AddScoped<IDbRepository, DbRepo>();
            services.AddScoped<IWorkLogic, WorkLogic>();
            services.AddScoped<IUserProfileLogic, UserProfileLogic>();
            services.AddScoped<IUserFinanceLogic, UserFinanceLogic>();
            services.AddTransient<DbInitializer>();

            // DO NOT redirect to login page on access denied, just return 401. Redirects to be handled from frontend based on HTTP status codes
           
            // Add framework services.
            services.AddCors();
            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddHttpClient();
 
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, DbInitializer dbInit)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            loggerFactory.AddConsole(_config.GetSection("Logging"));
            loggerFactory.AddDebug();
            loggerFactory.AddProvider(new Logger());
            //dbInit.Seed();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseCors(builder =>
       builder.WithOrigins("http://127.0.0.1:5500").AllowAnyHeader().AllowAnyMethod().AllowCredentials());
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
