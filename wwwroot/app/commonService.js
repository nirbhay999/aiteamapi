﻿define(['shared'], function (SHARED) {
    return commonService = {
        loader: {
            show: function (message) {
                Swal.fire({
                    html: '<div class="preloader-wrapper active">' +
                        '<div class="spinner-layer spinner-red-only">' +
                        '<div class="circle-clipper left">' +
                        '<div class="circle"></div>' +
                        '</div> <div class="gap-patch">' +
                        '<div class="circle"></div>' +
                        '</div><div class="circle-clipper right">' +
                        '<div class="circle"></div>' +
                        '</div></div></div >' +
                        '<div class="loading-message">' + message + '</div>',
                    showConfirmButton: false,
                    allowOutsideClick: false
                });
            },
            close: function () {
                Swal.close();
            }
        },
        error: function (message) {
            Swal.fire({
                position: 'top',
                type: 'error',
                title: message,
                showConfirmButton: false,
                timer: 1500,
                toast: true,
                width: '20rem'
            });
        },
        showSuccess: function (heading, message) {
            return Swal.fire(
                heading,
                message,
                'success'
            );
        }, 
        userProfile: {
            exists: function () {
                var ret = $.Deferred();
                var url = CONFIG.baseUrlApi + "userprofile";
                $.ajax(url, {
                    method: 'GET',
                }).done(result => ret.resolve(true, result)).fail(error => ret.resolve(false, error));
                return ret;
            },
            updateClientSide: function (profileData) {
                SHARED.userProfileCreated(true);
                SHARED.defaultName(profileData.name);
                if (profileData.profileImage) {
                    SHARED.profileImage(CONFIG.profileImagePath + profileData.profileImage);
                }
                SHARED.trial(!profileData.isPaidMember);
            }
        },
        showPaymentPopUp: function () {
            Swal.fire({
                html:
                    '<div id="dialog-replace">'+
                    '<div>Charges Breakup for full membership</div>' +
                    '<table style="margin:30px 5px;font-size:0.8em"><tbody><tr><td>AWS Cloud Charges</td><td>&#x20B9; 2200</td></tr><tr><td>On Boarding Charges</td><td>&#x20B9; 200</td></tr><tr><td>AWS Cloud Charges</td><td> &#x20B9; 2200</td></tr><tr><td><strong>Total<strong></td><td><strong>&#x20B9; 2543</strong></td></tr></tbody></table>' +
                    '<div class="row">'+
                     '<div class= "col s7">'+
                       '<div class="input-field">'+
                         '<input id="discountCode" type="text" class="validate">'+
                         '<label for="discountCode">Promo Code(optional)</label>'+
                       '</div>'+
                    '</div>' +
                    '<div class="col s5">' +
                    '<button class="btn z-depth-0" style="margin-top:20px;width:80px" id="checkCode"><span style="display:none"><i class="fas fa-circle-notch fa-spin"></i></span><span>Verify</span></button>' +
                    '</div>' +
                    '<div id="code-info-text">Code applied succesfully.You get a deduction of 80% . Payment to be done = <strong>&#x20B9;2200</strong></div>' +
                    '<div id="code-error-text"><i class="fas fa-exclamation-circle red-text" style="margin-right:6px;" ></i>Invalid code</div>' + 
                    '</div>' +
                   '</div>',
                showCloseButton: true,
                focusConfirm: true,
                confirmButtonText: "Go to Payment",
                onBeforeOpen: () => {
                    $("#checkCode").on("click", () => {
                        $("#checkCode").find("span").toggle();
                        $("#code-info-text").fadeOut(100);
                        $("#code-error-text").fadeOut(100);
                        var url = CONFIG.baseUrlApi + "UserFinance/CheckDiscountCode/" + $("#discountCode").val()
                        $.ajax(url, {
                            method: 'GET',
                        }).done(result => {
                            console.log("done")
                            CONFIG.encrypt = result.item3;
                            $("#code-info-text").fadeIn(500);
                        }).fail(error => {
                            console.log(error);
                            $("#code-error-text").fadeIn(500);
                        }).always(() => {
                            $("#checkCode").find("span").toggle();
                        })
                    })
                }
            }).then(result => {
                if (result) {
                    //commonService.loader.show("Connecting to PayTM");
                    var url = CONFIG.baseUrlApi + "userfinance" + "/OpenPayment?encryptedAmount="+CONFIG.encrypt;
                    $.ajax(url).done(result => {
                        //commonService.loader.close();
                        $("#dialog-replace").html(result);
                        $("#f2").submit();
                    }).fail(error => {
                        //commonService.loader.close();
                        commonService.error("Error while connecting to PayTM");
                    });
                }
            });
        }
    }
});