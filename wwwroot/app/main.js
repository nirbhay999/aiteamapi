﻿requirejs.config({
    paths: {
        'text': '../lib/require/text',
        'durandal': '../lib/durandal/js',
        'plugins': '../lib/durandal/js/plugins',
        'transitions': '../lib/durandal/js/transitions',
        'knockout': 'https://cdnjs.cloudflare.com/ajax/libs/knockout/3.5.0/knockout-min',
        'shared': 'shared',
        'commonService':'commonService',
        'config':'../js/config',
        'jquery': '../js/jquery-3.2.1.min',
        'materialize': '../lib/materialize-src/js/compiled/materialize',
        'chartBundle': 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min',
        'chart': 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min'
    },
    shim: {

       //'iziModal':{
       //    deps: ['jquery']
       //}
       //'metisMenu':{
       // deps: ['jquery']
        //}

    }
});

define(['durandal/system', 'durandal/app', 'durandal/viewLocator'],  function (system, app, viewLocator) {
    //>>excludeStart("build", true);
    system.debug(true);
    //>>excludeEnd("build");

    app.title = 'Sandear Technologies';

    app.configurePlugins({
        router:true,
        dialog: true
    });

    app.start().then(function() {
        //Replace 'viewmodels' in the moduleId with 'views' to locate the view.
        //Look for partial views in a 'views' folder in the root.
        viewLocator.useConvention();

        //Show the app by setting the root view model for our application with a transition.
        app.setRoot('viewmodels/shell');
    });
});