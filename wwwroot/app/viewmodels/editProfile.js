﻿define(['knockout', 'shared', 'commonService', 'plugins/router'], function (ko, shared, commonService, router) {
    var editProfileViewModel = function () {
        // Personal Details
        this.firstName = ko.observable("");
        this.lastName = ko.observable("");
        this.pinCode = ko.observable("");
        this.teamId = ko.observable("");

        // Bank details
        this.ifscCode = ko.observable("");
        this.accountNumber = ko.observable("");
        this.confirmAccountNumber = ko.observable("");
        this.accountHolderName = ko.observable("");
        this.ifscSearch = ko.observable(false);
        this.bankDetails = ko.observable("");

        this.attached = function () {
            $("#profile-image").hide();
            var self = this;
            var regex = /^[A-Za-z]{4}\d{7}$/;
            this.ifscSearch(false);
            this.ifscCode.subscribe(newVal => {
                this.bankDetails("");
                if (newVal.match(regex)) {
                    this.ifscSearch(true);
                    var url = "https://ifsc.razorpay.com/" + newVal;
                    $.ajax(url, { xhrFields: { withCredentials: false } }).done(result => {
                        console.log(result)
                        this.bankDetails(result.BANK + ", " + result.BRANCH + ", " + result.CENTRE);

                    }).fail(error => {
                        if (error.status === 404) {
                            commonService.error("Invalid IFSC code");
                        } else {
                            commonService.error(error.responseJSON);
                        }
                        ;
                    }).always(() => {
                        this.ifscSearch(false);
                    })
                }

            });

            // get existing bank details
            var url = CONFIG.baseUrlApi + "bankdetail";
            $.ajax(url, {
                method: 'GET',
            }).done(result => {
                console.log(result);
                if (result) {
                    self.accountNumber(result.accountNumber);
                    self.confirmAccountNumber(result.accountNumber);
                    self.accountHolderName(result.accountHolderName);
                    self.ifscCode(result.ifscCode);
                    M.updateTextFields();
                }
            }).fail(error => {
                console.log(error);
            });

            //get the user profile

            commonService.userProfile.exists().done((result,data) => {
                console.log(data);
                self.firstName(data.name.split(" ")[0]);
                self.lastName(data.name.split(" ")[1]);
                self.pinCode(data.pincode);
                self.teamId(data.ownTeamId);
                if (data.profileImage) {
                    $('#profile-image')
                        .attr('src', CONFIG.profileImagePath + data.profileImage).show();
                }
                M.updateTextFields();
            }).fail(error => {
                console.log(error);
            })
        }

        this.saveUserProfile = function () {
            var self = this;
            if (!self.firstName()) {
                commonService.error("First Name is mandatory");
                return;
            }
            if (!self.lastName()) {
                commonService.error("Last Name is mandatory");
            }
            if (!self.pinCode()) {
                commonService.error("Pincode is mandatory");
            }
            this.postProfileData();
        }

        this.postProfileData = function () {
            commonService.loader.show("Updating your profile");
            var self = this;
            var url = CONFIG.baseUrlApi + "userprofile";
            $.ajax(url, {
                contentType: 'application/json',
                data: JSON.stringify({
                    "Name": self.firstName() + " " + self.lastName(),
                    "Pincode": self.pinCode(),
                    "ProfileImage": $('#profile-image').attr('src')
                }),
                xhrFields: {
                    withCredentials: true
                },
                method: 'PUT',
            }).done(result => {
                commonService.loader.close();
                commonService.userProfile.updateClientSide(result);
                commonService.showSuccess("Success", "Profile updated successfully");
            }).fail(error => {
                commonService.loader.close();
                commonService.error("Error Occured while updating profile");
            });
        }


        this.saveBankDetails = function () {
            var self = this;
            if (!this.accountNumber()) {

                commonService.error("Account number is mandatory");
                return;
            }
            if (!this.confirmAccountNumber()) {
                commonService.error("Re-enter Account number is mandatory");
                return;
            }
            if (!this.accountHolderName()) {
                commonService.error("Account holder's name is mandatory");
                return;
            }
            if (!this.ifscCode()) {
                commonService.error("IFSC code is mandatory");
                return;
            }
            if (!this.bankDetails()) {
                commonService.error("Invalid IFSC code");
                return;
            }
            if (this.accountNumber() !== this.confirmAccountNumber()) {
                commonService.error("Account numbers do not match");
                return;
            }
            commonService.loader.show("Saving Bank Details")
            var url = CONFIG.baseUrlApi + "BankDetail";
            $.ajax(url, {
                contentType: 'application/json',
                data: JSON.stringify({
                    "IFSCCode": self.ifscCode(),
                    "AccountNumber": self.accountNumber(),
                    "AccountHolderName": self.accountHolderName()
                }),
                xhrFields: {
                    withCredentials: true
                },
                method: 'POST',
            }).done(result => {
                commonService.loader.close();
                commonService.showSuccess("Success","Bank details saved successfully")
            });
        }
    }

    this.readURL = function (input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#profile-image')
                    .attr('src', e.target.result).show();

            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    return editProfileViewModel;
});