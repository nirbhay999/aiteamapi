﻿define(['durandal/app', 'knockout', 'plugins/router', 'shared', 'commonService'], function (app, ko, router, SHARED, commonService) {
    var doWorkViewModel = function () {
        this.videoList = ko.observableArray([]);
        this.videoId = ko.observable("");
        this.videoDesc = ko.observable();
        this.videoCounter = ko.observable(0);
        this.videoContainerVisible = ko.observable(true);
        this.saveWorkVisible = ko.observable(false);
        this.numberOfVideos = ko.observable(5);
        this.videoError = ko.observable(false);
        this.videoUrl = ko.computed(function () {
            return "https://www.youtube.com/embed/" + this.videoId() + "?modestbranding=1&rel=0&showinfo=0&enablejsapi=1&origin=https://localhost:44348";
        }, this);

        this.canActivate = function () {
            if (!SHARED.workId) {
                router.navigate("#work");
                return false;
            }
            return true;
        }

        this.activate = function () {  
            this.videoList(SHARED.videoList);
            this.videoId(this.videoList()[0]);
            this.numberOfVideos(this.videoList().length);
            this.videoCounter(this.videoCounter() + 1);
        }

        this.attached = function () {

            $("#video-pre-loader").show();
            $("#video-player").hide();

            // Initiate youtube player
            var tag = document.createElement('script');
            tag.id = 'iframe-demo';
            tag.src = 'https://www.youtube.com/iframe_api';
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

       

            //Init materialize inputs
            $("#inputObject").chips({
                placeholder: 'Enter objects',
                secondaryPlaceholder: '+Object',
            });
            $("#inputAction").chips({
                placeholder: 'Enter actions',
                secondaryPlaceholder: '+Action',
            });
            $('textarea#textarea2').characterCounter();

        }
        this.nextClicked = function () {
           
            var objects = M.Chips.getInstance(document.getElementById("inputObject")).chipsData;
            var actions = M.Chips.getInstance(document.getElementById("inputAction")).chipsData;
            if (!this.videoError()) {
                if (objects.length === 0 || actions.length === 0 || !this.videoDesc()) {
                    commonService.error("All fileds are mandatory");
                    return;
                }
            } else {
                objects.push({ tag: "error" });
                actions.push({ tag: "error" });
                this.videoDesc("error");
            }
            $("#video-player").hide();
            $("#video-pre-loader").show();
            if (this.videoCounter() === this.videoList().length) {
                this.videoContainerVisible(false);
                this.saveWorkVisible(true);
                return
            }

            // save the video response to database
            var self = this;
            
            var actualObjects = [];
            for (var i = 0; i < objects.length; i++) {
                actualObjects.push(objects[i].tag);
            }
            
            var actualActions = [];
            for (var i = 0; i < actions.length; i++) {
                actualActions.push(actions[i].tag);
            }
            var url = CONFIG.baseUrlApi + "work/SaveVideoResponse";
            $.ajax(url, {
                contentType: 'application/json',
                data: JSON.stringify({
                    "VideoId": self.videoId(),
                    "Objects": actualObjects.join('|'),
                    "Actions": actualActions.join('|'),
                    "Description": self.videoDesc()
                }),
                method: 'POST',
            }).always(function (data, status, xhr) {
                if (xhr.status === 200) {
                    
                } else {
                    // TODO error
                }

                self.videoId(self.videoList()[self.videoCounter()]);
                self.videoCounter(self.videoCounter() + 1)
                self.videoError(false);
                self.videoDesc("");
                objects = []
                actions = [];
                M.updateTextFields();
                M.updateCharacterCounter();
                var actionChip = M.Chips.getInstance(document.getElementById("inputObject"));
                for (var i = 0; i < objects.length; i++) {

                }
                for (var j = 0; j < actions.length; j++) {

                }
                
            });

        }
        this.saveAndSendForVerification = function () {
            // save the video response to database
            var self = this; 
            var url = CONFIG.baseUrlApi + "work/SaveWork";
            $.ajax(url, {
                contentType: 'application/json',
                data: JSON.stringify(self.currentWork),
                method: 'POST',
            }).always(function (data, status, xhr) {
                if (xhr.status === 200) {
                    alert("work completed");
                    self.workList.remove(self.currentWork);
                    $('#myModal').modal('hide');
                } else {
                    // TODO error
                }
            });

        }
    }

    return doWorkViewModel;
});

var player;
function onYouTubeIframeAPIReady() {
    player = new YT.Player('video-player', {
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
    function onPlayerReady(event) {
        if (event.target.j.videoUrl === "https://www.youtube.com/watch") {

        }
        else {
            console.log("ready");
            $("#video-pre-loader").hide();
            $("#video-player").show();
        }
    }

    function onPlayerStateChange(event) {
        console.log("state changed");
    }
}


