﻿define(['durandal/app', 'knockout', 'plugins/router','shared'], function (app, ko,router,SHARED) {
    var workViewModel = function () {
        this.workList = ko.observableArray([]);
        this.activate = function () {
            this.getWork();
        };

        this.attached = function () {
        }

        this.workClicked = function (data, event) {
            SHARED.workId = data.id;
            SHARED.videoList = data.videosStr.split(";");
            router.navigate("#dowork");
        }
        
        this.getWork = function () {
            var self = this;
            var url = CONFIG.baseUrlApi + "work/GetWork";
            $.ajax(url, {
                method: 'GET',
            }).always(function (data, status, xhr) {
                console.log(data, status, xhr);
                self.workList(data);
            });
        }
        this.animateEntry = function (element, index) {
            $(element).hide().fadeIn(500);
        };
      
    }
    return workViewModel;
});

