﻿define(['durandal/app', 'knockout'], function (app, ko) {
    var supportViewModel = function () {
        this.complainDesc = ko.observable("");
        this.selectedCategory = ko.observable("");
        this.userTickets = ko.observableArray([]);
        this.attached = function () {
            $('select').formSelect();
            $('.collapsible').collapsible();
        }
        this.postComplain = function () {
            var self = this;
            var url = CONFIG.baseUrlApi + "complain/CreateComplain";
            $.ajax(url, {
                contentType: 'application/json',
                data: JSON.stringify({
                    "Category": self.selectedCategory(),
                    "Description": self.complainDesc()
                }),
                method: 'POST',
            }).always(function (data, status, xhr) {
                if (xhr.status === 200) {
                    alert("complain created");
                } else {
                    // TODO error
                }
            });
        }

        this.getComplains = function () {
            var self = this;
            var url = CONFIG.baseUrlApi + "complain/GetUserComplains";
            $.ajax(url, {
                method: 'GET',
            }).always(function (data, status, xhr) {
                if (xhr.status === 200) {
                    console.log(data);
                    for(item of data) {
                        item.status = CONFIG.complainStatus[item.status];
                        item.category = CONFIG.complainCategory[item.category];
                        if (item.status === 'Resolved') {
                            item.showReopenButton = true;
                        }
                        else {
                            item.showReopenButton = true;
                        }
                    }
                    self.userTickets(data);
                }
            });
        }

        this.reopenTicket = function (data) {

            delete data.showReopenButton;
            data.status = CONFIG.complainStatus.indexOf(data.status);
            var url = CONFIG.baseUrlApi + "complain/ReopenComplain";
            $.ajax(url, {
                contentType: 'application/json',
                data: JSON.stringify(data),
                method: 'POST',
            }).always(function (data, status, xhr) {
                if (xhr.status === 200) {
                    alert("complain reopened");
                } else {
                    // TODO error
                }
            });
        };

    }

    return supportViewModel;
});