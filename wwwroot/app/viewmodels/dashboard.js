﻿define(['durandal/app', 'knockout', 'commonService','shared'], function (app, ko, commonService, SHARED) {
    var dashBoardViewModel = function () {
        this.top10Trainers = ko.observableArray([
            {
                name: 'Aneesh Rajmi',
                img: '../../images/avatars/user-01.jpg',
                videoMinutes: '120',
                earning: '2200',
                bonus: '900'
            },
            {
                name: 'Aneesh Rajmi',
                img: '../../images/avatars/user-01.jpg',
                videoMinutes: '120',
                earning: '2200',
                bonus: '900'
            },
            {
                name: 'Aneesh Rajmi',
                img: '../../images/avatars/user-01.jpg',
                videoMinutes: '120',
                earning: '2200',
                bonus: '900'
            },
            {
                name: 'Aneesh Rajmi',
                img: '../../images/avatars/user-01.jpg',
                videoMinutes: '120',
                earning: '2200',
                bonus: '900'
            },

        ]);

        this.activate = function () {
            

        }


         
        this.attached = function () {
            if (SHARED.userProfileCreated()) {
                // todo fetch details of specific profile to show on "My Dashboard". Use profileData
            }
            drawLineChart1();
            drawObjectChart();
            drawPieChart();
            drawHorizontalBar();
            drawLineChart2();
        }


        function drawLineChart1() {
            var ctx = document.getElementById("chart");
            var data = {
                labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May','Jun', 'Jul','Aug','Sep','Oct','Nov','Dec'],
                datasets: [{
                    data: [86, 904, 106, 106, 107,65,90,170,200,50,89,135],
                    label: "Trained",
                    borderColor: "#3e95cd",
                    fill: false
                }, {
                    data: [282, 350, 411, 502, 635,150,200,100,400,700,230,600],
                    label: "Verified",
                    borderColor: "#8e5ea2",
                    fill: false
                }]
            }
            new Chart(ctx, {
                type: 'line',
                data: data,
                options: {
                    elements: {
                        line: {
                            tension: 0, // disables bezier curves
                        }
                    },
                    animation: {
                        duration: 2000,
                        easing: 'easeOutQuart'
                    }
                }
            });
        }
        function drawObjectChart() {
            new Chart(document.getElementById("bar-chart"), {
                type: 'bar',
                data: {
                    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                    datasets: [
                        {
                            label: "Object Identified",
                            backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850"],
                            data: [2478, 5267, 734, 784, 433,890,1050,2190,1500,600,1800,2300]
                        }
                    ]
                },
                options: {
                    legend: { display: false },
                    title: {
                        display: true,
                        text: 'Unique objects identified in past 5 months'
                    }
                }
            });
        }
        function drawPieChart() {
            new Chart(document.getElementById("pie-chart"), {
                type: 'pie',
                data: {
                    labels: [">5", ">2", "0"],
                    datasets: [{
                        label: "Population (millions)",
                        backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f"],
                        data: [2478, 5267, 734]
                    }]
                },
                options: {
                    title: {
                        display: false,
                        text: 'Predicted world population (millions) in 2050'
                    },
                    animateScale: true
                }
            });
        }
        function drawHorizontalBar() {
            new Chart(document.getElementById("bar-chart-horizontal"), {
                type: 'horizontalBar',
                data: {
                    labels: ["Jan", "Feb", "Mar", "Apr", "May"],
                    datasets: [
                        {
                            label: "Actions identified",
                            backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850"],
                            data: [2478, 5267, 734, 784, 433]
                        }
                    ]
                },
                options: {
                    legend: { display: false },
                    title: {
                        display: false,
                        text: 'Predicted world population (millions) in 2050'
                    }
                }
            });
        }
        function drawLineChart2() {
            new Chart(document.getElementById("line-chart2"), {
                type: 'line',
                data: {
                    labels: [1500, 1600, 1700, 1750, 1800, 1850, 1900, 1950, 1999, 2050],
                    datasets: [{
                        data: [86, 114, 106, 106, 107, 111, 133, 221, 783, 2478],
                        label: "Actions",
                        borderColor: "#3e95cd",
                        fill: false
                    }, {
                        data: [282, 350, 411, 502, 635, 809, 947, 1402, 3700, 5267],
                        label: "Summary",
                        borderColor: "#8e5ea2",
                        fill: false
                    }
                    ]
                },
                options: {
                    title: {
                        display: false,
                        text: 'Action and summary co-realtion'
                    }
                }
            });
        }
    };
    return dashBoardViewModel;
});