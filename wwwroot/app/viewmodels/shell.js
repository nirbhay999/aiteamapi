﻿define(['plugins/router', 'durandal/app', 'materialize', 'shared', 'knockout', 'chartBundle', 'chart', 'config', 'commonService'], function (router, app, m, SHARED, ko, cb, ch, c, commonService) {
    return {
        router: router,
        contentVisible: ko.observable(false),
        activate: function () {
            router.map([
                { route: '', title: 'Dashboard', moduleId: 'viewmodels/dashboard', nav: true },
                { route: 'Earnings', moduleId: 'viewmodels/earnings', nav: true },
                { route: 'VerificationTasks', moduleId: 'viewmodels/workVerification', nav: true },
                { route: 'work', moduleId: 'viewmodels/work', nav: true },
                { route: 'profile', moduleId: 'viewmodels/editprofile', nav: true },
                { route: 'Support', moduleId: 'viewmodels/support', nav: true },
                { route: 'resetPswd', moduleId: 'viewmodels/resetPswd', nav: true },
                { route: 'ActivateProfile', moduleId: 'viewmodels/activateProfile', nav: false },
                { route: 'PaymentResult', moduleId: 'viewmodels/paymentResult', nav: false },
                { route: 'DoWork', moduleId: 'viewmodels/dowork', nav: false }

            ]).buildNavigationModel();

            return router.activate();
        },
        attached: function () {
            $('.sidenav').sidenav(); // Initiate material sidenav
            var cont = $("body");
            $('.dropdown-trigger').dropdown({

                coverTrigger: false,
                constrainWidth: false,
                container: cont

            }); // Init notifications-dropdown
            // set up global ajax hooks
            $.ajaxSetup({
                xhrFields: {
                    withCredentials: true
                }
            }).complete = function (response) {
                if (response.status === 401) {
                    window.location.href = CONFIG.loginPageUrl;
                }
            };

            commonService.userProfile.exists().done((result, profileData) => {
                if (result) // user profile exists
                {
                    commonService.userProfile.updateClientSide(profileData);
                }
            });

        },
        logout: function () {
            var self = this;
            var url = CONFIG.baseUrlApi + "auth/logout";
            $.ajax(url, {
                method: 'POST',
            }).always(function (data, status, xhr) {
                console.log(data, status, xhr);
                if (xhr.status === 200) {
                    window.location.href = CONFIG.baseURL
                } else {

                }
            });
        },
        openPaymentPopup: function () {
            commonService.showPaymentPopUp();
        }
    };
});