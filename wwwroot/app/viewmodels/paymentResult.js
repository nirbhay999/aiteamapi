﻿define(['durandal/app', 'knockout'], function (app, ko) {

    var paymentResultViewModel = function () {
        this.success = ko.observable(true);
        this.activate = function (params) {
            if (params.respcode === "01") {
                this.success(true);
            }
            else {
                this.success(false);
            }
        }
        this.attached = function () {
           
        }
    };
    return paymentResultViewModel;
});