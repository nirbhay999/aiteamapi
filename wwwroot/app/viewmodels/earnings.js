﻿define(['durandal/app', 'knockout'], function (app, ko) {
    var paymentViewModel = function () {
        this.unverifiedWorks = ko.observableArray([]);
        this.verifiedWorks = ko.observableArray([]);
        this.encashmentRequests = ko.observableArray([]);
        this.encashableAmount = ko.observable(0);
        this.activate = function () {
            this.getCompletedWorks();
        }

        this.getCompletedWorks = function () {
            var self = this;
            var url = CONFIG.baseUrlApi + "UserFinance/GetCompletedWorks";
            $.ajax(url, {
                method: 'GET',
            }).always(function (data, status, xhr) {
                if (xhr.status === 200) {
                    self.unverifiedWorks(data.filter(function (item) {
                       return item.status === 0;
                    }));

                    self.verifiedWorks(data.filter(function (item) {
                        return item.status === 1;
                    }));
                    console.log(self.unverifiedWorks());
                    console.log(self.verifiedWorks());
                    self.encashableAmount(self.verifiedWorks().reduce(function (acc, val) { return acc + val.payments; }, 0));
                } else {
                    // TO DO error
                }
            });

        }

        this.getEncashmentRequests = function () {
            var self = this;
            var url = CONFIG.baseUrlApi + "UserFinance/GetEncashmentRequests";
            $.ajax(url, {
                method: 'GET',
            }).always(function (data, status, xhr) {
                if (xhr.status === 200) {
                    console.log(data);
                }
            });
        }

        this.raiseEncashmentRequest = function () {
            var self = this;
            var url = CONFIG.baseUrlApi + "UserFinance/CreateEncashmentRequest";
            $.ajax(url, {
                method: 'GET',
            }).always(function (data, status, xhr) {
                if (xhr.status === 200) {
                    alert("request created");
                }
            });

        }

    }
    return paymentViewModel
});