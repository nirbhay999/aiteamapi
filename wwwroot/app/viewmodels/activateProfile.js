﻿define(['knockout', 'shared', 'commonService', 'plugins/router'], function (ko, shared, commonService, router) {
    var activateProfileViewModel = function () {
        this.firstName = ko.observable("");
        this.lastName = ko.observable("");
        this.pinCode = ko.observable("");
        this.verifierTeamId = ko.observable("");
        this.email = ko.observable("");
        this.trainerName = ko.observable("");
        this.trainerFind = ko.observable(false);
        this.isTeamIdValid = false;

        this.attached = function () {
            var self = this;
            $('.datepicker').datepicker({ yearRange: [1970, 2005] });
            $("#profile-image").hide();
            this.verifierTeamId.subscribe(function (newVal) {
                self.trainerName("");
                self.isTeamIdValid = false;
                if (newVal.length === 8) {
                    self.trainerFind(true);
                    var url = CONFIG.baseUrlApi + "userprofile" + "/GetTrainer/" + newVal;
                    $.ajax(url).always(function (data, status, xhr) {
                        console.log(data, status, xhr);
                        if (xhr.status === 200) {
                            self.trainerName(data.name);
                            self.isTeamIdValid = true;
                        } else {
                            self.isTeamIdValid = false;
                            Swal.fire({
                                position: 'top',
                                type: 'error',
                                title: 'Trainer Team Id not found',
                                showConfirmButton: false,
                                timer: 3000,
                                toast: true,
                                width: '20rem'
                            });
                        }
                        self.trainerFind(false);
                    });
                }
            });
        }

        this.activateProfile = function (viewModel, event) {
            if (!this.firstName() || !this.lastName() || !this.pinCode() || !this.verifierTeamId() || !this.email()) {
                commonService.error("All fields are mandatory !");
                return;
            }

            if (!this.isTeamIdValid) {
                commonService.error("Team Id is not valid");
                return;
            }
            this.postProfileData().done(result => {
                commonService.userProfile.updateClientSide(result);
                if (event.currentTarget.id === "paid") {
                    this.connectToPaytm();
                } else {

                    commonService.showSuccess("Trial profile activated!", "Click ok to go to dashboard.").then(() => {
                        router.navigate("#");
                    })
                }
            }).fail(error => {
                // do nothing error handling already done in this.postProfileData
            });
        }

        this.connectToPaytm = function () {
            commonService.loader.show("Connecting to PayTM");
            var url = CONFIG.baseUrlApi + "userfinance" + "/OpenPayment";
            $.ajax(url).done(result => {
                commonService.loader.close();
                $("#replace").html(result);
                $("#f2").submit();
            }).fail(error => {
                commonService.loader.close();
                commonService.error("Error while connecting to PayTM");
            });

        }

        this.postProfileData = function () {
            var ret = $.Deferred();
            commonService.loader.show("Creating your profile");
            var self = this;
            var url = CONFIG.baseUrlApi + "userprofile";
            $.ajax(url, {
                contentType: 'application/json',
                data: JSON.stringify({
                    "Name": self.firstName() + " " + self.lastName(),
                    "EmailId": self.email(),
                    "Pincode": self.pinCode(),
                    "VerifierTeamId": self.verifierTeamId(),
                    "ProfileImage": $('#profile-image').attr('src')
                }),
                xhrFields: {
                    withCredentials: true
                },
                method: 'POST',
            }).done(result => {
                commonService.loader.close();
                ret.resolve(result)
            }).fail(error => {
                commonService.loader.close();
                commonService.error("Error Occured while creating profile");
                ret.reject(error)
            });
            return ret;
        }



    }

    this.readURL = function (input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#profile-image')
                    .attr('src', e.target.result).show();

            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    return activateProfileViewModel;
});

