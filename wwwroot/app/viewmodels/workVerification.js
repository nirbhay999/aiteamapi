﻿define(['durandal/app', 'knockout'], function (app, ko) {

    var verificationViewModel = function () {
        this.verificationWorkParts = ko.observableArray([]);
        this.videoId = ko.observable("");
        this.objects = ko.observableArray([]);
        this.actions = ko.observableArray([]);
        this.description = ko.observable("");
        this.workId = ko.observable("");
        this.videoUrl = ko.computed(function () {
            return "https://www.youtube.com/embed/" + this.videoId() + "?modestbranding=1&rel=0&showinfo=0&enablejsapi=1&origin=http://localhost:26832";
        }, this);
        this.activate = function () {
            this.getVerificationWorkPartsForUser();
        }
        this.attached = function () {
            $("#modalVerify").modal({
                onOpenStart: function () {
                    $("#video-pre-loader").show();
                },
                startingTop: "0%",
                endingTop: "0%",
                dismissible: false,
                opacity: 0.9
            });
        }
        this.openVerificationWork = function (data) {
            this.workId(data.verificationWorkId);
            this.videoId(data.videoId);
            var self = this;

            var url = CONFIG.baseUrlApi + "work/GetVideoResponse";
            $.ajax(url, {
                contentType: 'application/json',
                data: JSON.stringify({
                    "VideoId": self.videoId(),
                    "Username": data.verificationWork.username
                }),
                method: 'POST',
            }).always(function (data, status, xhr) {
                if (xhr.status === 200) {
                    self.objects(data.objects.split('|'));
                    self.actions(data.actions.split('|'));
                    self.description(data.description);
                    $('#modalVerify').modal('open');
                } else {
                    // TODO error
                }
            });
        }
        this.completeVerification = function (data) {
            var self = this;
            var url = CONFIG.baseUrlApi + "work/VerifyWork";
            $.ajax(url, {
                contentType: 'application/json',
                data:JSON.stringify(self.workId()),
                method: 'POST',
            }).always(function (data, status, xhr) {
                if (xhr.status === 200) {
                    alert("Work verified succcesfully");
                } else {
                    // TODO error
                }
            });
        }

        this.reject = function (data) {
            var self = this;
            var url = CONFIG.baseUrlApi + "work/RejectWork";
            $.ajax(url, {
                contentType: 'application/json',
                data: JSON.stringify(self.workId()),
                method: 'POST',
            }).always(function (data, status, xhr) {
                if (xhr.status === 200) {
                    alert("Work rejected succcesfully");
                } else {
                    // TODO error
                }
            });
        }

        this.getVerificationWorkPartsForUser = function() {
            var self = this;
            var url = CONFIG.baseUrlApi + "work/GetVerificationWorks";
            $.ajax(url, {
                method: 'GET',
            }).always(function (data, status, xhr) {

                if (data.length > 0) {
                    for (var i = 0 ;i< data.length;i++)
                    {
                        var dateObj=new Date(data[i].assignedDate);
                        data[i].onlyDate =dateObj.getDate();
                        data[i].onlyMonth = CONFIG.monthNames[dateObj.getMonth()];
                        data[i].onlyYear = dateObj.getFullYear();
                        data[i].onlyDay = CONFIG.dayNames[dateObj.getDay()];

                    }
                    self.verificationWorkParts(data);
                }

            });

        }
    };
    return verificationViewModel;
});