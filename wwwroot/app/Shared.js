﻿define(['knockout'], function (ko) {
    return SHARED = {
        profileImage: ko.observable("../../images/defaultProfileImage.png"),
        defaultName: ko.observable("Guest User"),
        trial: ko.observable(false),
        userProfileCreated: ko.observable(false),
        workId: "",
        videoList:[]

    }
});

// *******************************************
//**********************MUST READ*************
// This file stored variables that can change during program execution.
// For constants store it in config.js