   var CONFIG = {
        baseUrlApi: 'https://localhost:44348/api/',
        redirectPageAfterLogin: '/app.html',
        loginPageUrl: '/index.html#login',
        regPageUrl: '/index.html#registration',
        baseURL: 'https://localhost:44348/',
        monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"],
        dayNames: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        complainCategory: ["Payment", "Work", "Verification", "Others"],
        complainStatus: ["In Progress", "Resolved", "Re Opened"],
        profileImagePath:'"../../images/ProfilePictures/'
    }
