﻿function LoginViewModel() {

    // Login
    this.mobileNumber = ko.observable("");
    this.password = ko.observable("");
    this.rememberMe = ko.observable(false);

    // Registration
    this.mobileNumberContainerVisible = ko.observable(true);
    this.OTPContainerVisible = ko.observable(false);
    this.passwordContainerVisible = ko.observable(false);
    this.newMobileNumber = ko.observable("");
    this.regOTP = ko.observable("");
    this.newPassword = ko.observable("");
    this.confirmNewPassword = ko.observable("");
    this.OTP = ko.observable("");
    this.errorText = ko.observable("");
    this.regDialogClosed = function () {
        window.location.hash = "close";
        this.mobileNumberContainerVisible(true);
        this.OTPContainerVisible(false);
        this.passwordContainerVisible(false);
        this.newMobileNumber("");
        this.regOTP("");
        this.newPassword("");
        this.confirmNewPassword("");
        this.OTP("");
        this.errorText("");
    }

    //Forgot password
    this.confirmPassword = ko.observable("");

    this.error = ko.observable("");
    this.OTPSectionVisible = ko.observable(true);
    this.resetPswdSectionVisible = ko.observable(false);
    this.login = function () {
        var self = this;
        this.error("");
        var url = CONFIG.baseUrlApi + "auth/login";
        $.ajax(url, {
            contentType: 'application/json',
            data: JSON.stringify({
                "MobileNumber": this.mobileNumber(),
                "Password": this.password(),
                "RememberMe": this.rememberMe()
            }),
            method: 'POST',
            crossDomain: 'true'
        }).always(function (data, status, xhr) {
            console.log(data, status, xhr);
            if (xhr.status === 200) {
                window.location.href = CONFIG.redirectPageAfterLogin;
            } else {
                self.error(data.responseText);
            }
        });
    }

    this.getPasswordResetOTP = function () {
        var self = this;
        this.error("");
        var url = CONFIG.baseUrlApi + "auth/GetPasswordResetOTP";
        $.ajax(url, {
            contentType: 'application/json',
            data: this.mobileNumber(),
            method: 'POST',
        }).always(function (data, status, xhr) {
            if (xhr.status === 200) {
                self.OTPSectionVisible(false);
                self.resetPswdSectionVisible(true);
            }
        });
    }

    this.resetPassword = function () {
        var self = this;
        this.error("");
        var url = CONFIG.baseUrlApi + "auth/ResetPassword";
        $.ajax(url, {
            contentType: 'application/json',
            data: JSON.stringify({
                "MobileNumber": self.mobileNumber(),
                "OTP": self.OTP(),
                "Password": self.password(),
                "ConfirmPassword": self.confirmPassword()
            }),
            method: 'POST',
        }).always(function (data, status, xhr) {
            if (xhr.status === 200) {

            }
        });
    }

    this.getRegOTP = function () {
        var self = this;
        var url = CONFIG.baseUrlApi + "auth/Register";
        $.ajax(url, {
            contentType: 'application/json',
            data: self.newMobileNumber(),
            method: 'POST',
            crossDomain: 'true'
        }).always(function (data, status, xhr) {
            console.log(data, status, xhr);
            if (xhr.status === 200) {
                self.mobileNumberContainerVisible(false);
                self.OTPContainerVisible(true);
                self.passwordContainerVisible(false);
                self.errorText("");
            } else {
                self.errorText(data.responseText);
                self.newMobileNumber("");
            }
        });
    }
    this.editMobileNumber = function () {
        this.mobileNumberContainerVisible(true);
        this.OTPContainerVisible(false);
        this.passwordContainerVisible(false);
    }
  this.verifyRegOTP = function () {
        var self = this;
        var url = CONFIG.baseUrlApi + "auth/VerifyOTP";
        $.ajax(url, {
            contentType: 'application/json',
            data: self.newMobileNumber() + "." + self.regOTP(),
            method: 'POST',
            crossDomain: 'true'
        }).always(function (data, status, xhr) {
            console.log(data, status, xhr);
            if (xhr.status === 200) {
                self.mobileNumberContainerVisible(false);
                self.OTPContainerVisible(false);
                self.passwordContainerVisible(true);
                self.errorText("");
            } else {
                self.errorText(data.responseText);
                self.regOTP("");
            }
        });
    }

    this.submitPassword = function () {
        var self = this;
        var url = CONFIG.baseUrlApi + "auth/SetPassword";
        $.ajax(url, {
            contentType: 'application/json',
            data: JSON.stringify({
                "MobileNumber": self.newMobileNumber(),
                "Password": self.newPassword()
            }),
            method: 'POST',
            crossDomain: 'true'
        }).always(function (data, status, xhr) {
            console.log(data, status, xhr);
            if (xhr.status === 200) {
                self.errorText("");
                self.mobileNumberContainerVisible(false);
                self.OTPContainerVisible(false);
                self.passwordContainerVisible(false);
                window.location.href = CONFIG.redirectPageAfterLogin;
            } else {
                self.errorText(data.responseText);
                self.newPassword("");
                self.confirmNewPassword("");
            }
        });
    }


}
ko.applyBindings(new LoginViewModel());


function numbersOnly(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

$(document).ready(function () {
    $.ajaxSetup({
        xhrFields: {
            withCredentials: true
        }
    });
});