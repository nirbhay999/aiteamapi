﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AITeamAPI.Models
{
    public enum WorkStatus
    {
        PendingVerification=0,
        VerficationCompleted=1,
        PaymentReleased=2

    }

    public enum ComplaintType
    {
        Payment=0,
        Work=1,
        Verification=2,
        Others=3
    }

    public enum ComplainStatus
    {
        InProgress=0,
        Resolved=1,
        ReOpened=2
    }

}
