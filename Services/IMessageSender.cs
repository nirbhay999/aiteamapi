﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AITeamAPI.Services
{
    public interface IMessageSender
    {
        void SendSmsAsync(string number, string message);
    }
}
