﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AITeamAPI.Migrations
{
    public partial class DiscountCodesUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "DiscountCode",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Code",
                table: "DiscountCode");
        }
    }
}
