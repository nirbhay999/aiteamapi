﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AITeamAPI.Migrations
{
    public partial class teamIDunique : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "OwnTeamId",
                table: "UserProfiles",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_UserProfiles_OwnTeamId",
                table: "UserProfiles",
                column: "OwnTeamId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_UserProfiles_OwnTeamId",
                table: "UserProfiles");

            migrationBuilder.AlterColumn<string>(
                name: "OwnTeamId",
                table: "UserProfiles",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
