﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AITeamAPI.Migrations
{
    public partial class complain_Response_db : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Response",
                table: "UserComplains");

            migrationBuilder.CreateTable(
                name: "ComplainResponses",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ResponseTime = table.Column<DateTime>(nullable: false),
                    Response = table.Column<string>(nullable: true),
                    ComplainId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComplainResponses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ComplainResponses_UserComplains_ComplainId",
                        column: x => x.ComplainId,
                        principalTable: "UserComplains",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ComplainResponses_ComplainId",
                table: "ComplainResponses",
                column: "ComplainId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ComplainResponses");

            migrationBuilder.AddColumn<string>(
                name: "Response",
                table: "UserComplains",
                nullable: true);
        }
    }
}
