﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AITeamAPI.Entities;
using AITeamAPI.Repository;
using AITeamAPI.DTO;
using System.IO;
using System.Text;
using AITeamAPI.Models;
using Microsoft.Extensions.Configuration;

namespace AITeamAPI.BusinessLogic
{
    public class WorkLogic : IWorkLogic
    {
        IDbRepository _repo;
        private IConfiguration _config;
        const int MINVERIFIERS = 2;

        public WorkLogic(IDbRepository repo , IConfiguration config)
        {
            _config = config;
            _repo = repo;
        }


        public async Task<List<Work>[]> GetWork(int noOfVerifiers)
        {
            switch (noOfVerifiers)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                    var waitingCall = new List<Task<List<Work>>>();
                    waitingCall.Add(_repo.GetWork(3));
                    waitingCall.Add(_repo.GetWork(4));
                    waitingCall.Add(_repo.GetWork(5));
                    var result = await Task.WhenAll(waitingCall.ToArray());
                    return result;
                default:
                    var waitCall1= new List<Task<List<Work>>>();
                    waitCall1.Add(_repo.GetWork(noOfVerifiers + 1));
                    waitCall1.Add(_repo.GetWork(noOfVerifiers + 2));
                    waitCall1.Add(_repo.GetWork(noOfVerifiers + 3));
                    var result1 = await Task.WhenAll(waitCall1.ToArray());
                    return result1;
            }

        }

        // Todo function to generate dummy works which will have min number of verifiers greater than the number of verfiers that the user has

        public async Task<bool> SaveWork(Work work , string username)
        {
            // Get all the verifiers paid or trial
            var verfiers = await _repo.GetVerifiers(username);

            if (verfiers.Count == 0 || (verfiers.Count <  work.MinVerifiers))
            {
                return false;
            }
            VerificationWork verWork = new VerificationWork();
            //verWork.Id = work.Id;
            verWork.MinVerifiers = work.MinVerifiers;
            verWork.Payments = work.Payment;
            verWork.Status = WorkStatus.PendingVerification;
            verWork.Username = username;
            verWork.CompletionDate = DateTime.Today;


            Random rand = new Random();
            var listOfVerifiers = new List<VerificationWorkParts>();
            for(int i = 0; i < work.MinVerifiers;i++)
            {
                VerificationWorkParts verfier = new VerificationWorkParts();
                //verfier.VerificationWorkId = work.Id;
                verfier.Username = verfiers[i].UserName;  // To Do introduce some randomness in assigning verfiers
                //int randomVideoNumber= rand.Next(0, work.Videos.Count);
                //verfier.VideoId = work.Videos[randomVideoNumber].VideoId;
                verfier.AssignedDate = DateTime.Now;
                listOfVerifiers.Add(verfier);
            }
            verWork.Verfiers = listOfVerifiers;
            var x = await _repo.Add<VerificationWork>(verWork);
            if (x != null)
            {
                var result = await _repo.SaveAllAsync();
                return result;
            }
            return false;

        }


        public async Task<List<VerificationWorkParts>> GetVerificationWorkPartsByUsername(string username)
        {
            return await _repo.GetVerificationWorkPartsByUsername(username);
        }

        public async Task<List<VerificationWorkParts>> GetVerificationWorkPartsByWorkId(string workId, string username)
        {
            return await _repo.GetVerificationWorkPartsByWork(workId);

        }
        public async Task<bool> MarkWorkAsVerfied(string workId,string username)
        {
            var userProfile =await  _repo.GetEntityByPrimaryKeyAsync<UserProfile>(username);
            if (userProfile.IsPaidMember == false)  // trial account cannot verify a work
                return false;
            var verificationWorkParts = await _repo.GetVerificationWorkPartsByWork(workId);
            int totalVerificationsRequired = verificationWorkParts.Count;
            var userVerficationWorkPart = verificationWorkParts.Find(x => x.Username == username);
            userVerficationWorkPart.VerificationComplete = true;
            var numberOfCompletedVerifications = verificationWorkParts.Count(x => x.VerificationComplete == true);
            if (numberOfCompletedVerifications == totalVerificationsRequired)
            {
                // Marks the work as verification complete
                var verificationWork = await _repo.GetEntityByPrimaryKeyAsync<VerificationWork>(workId);
                verificationWork.CompletionDate = DateTime.Today;
                verificationWork.Status = WorkStatus.VerficationCompleted;
                _repo.Update<VerificationWork>(verificationWork);

            }
            _repo.Update<VerificationWorkParts>(userVerficationWorkPart);
            return await _repo.SaveAllAsync();

        }

        public async Task<bool> SaveVideoResponse(VideoResponses res)
        {
            var x = await _repo.Add<VideoResponses>(res);
            if (x != null)
            {
                var result = await _repo.SaveAllAsync();
                if (result == true)
                {
                    return true;
                }
            }
            return false;
        }

        private static IEnumerable<int> PartitionMeInto(int value, int count)
        {
            if (count <= 0) throw new ArgumentException("count must be greater than zero.", "count");
            var result = new int[count];

            int runningTotal = 0;
            for (int i = 0; i < count; i++)
            {
                var remainder = value - runningTotal;
                var share = remainder > 0 ? remainder / (count - i) : 0;
                result[i] = share;
                runningTotal += share;
            }

            if (runningTotal < value) result[count - 1] += value - runningTotal;

            return result;
        }

        public async Task<VideoResponses> GetVideoResponse(string username, string videoId)
        {
           return await _repo.GetVideoResponse(username, videoId);
        }

        private async Task<int> GetAmountOfCompletedWork(string username)
        {
            var verficationCompleteWorkAmount = await _repo.GetEarnedAndVerfiedAmount(username);
            var verificationPendingWorkAmount = await _repo.GetEarnedAndNotVerfiedAmount(username);
            return verficationCompleteWorkAmount + verificationPendingWorkAmount;

        }
        public async Task<int> GetEarnableAmount(string username, int dailyEarningLimit, int lifeTimeLimit)
        {
            var todaysEarnedAmount = await _repo.GetTodaysCompletedWorkAmount(username);
            if (todaysEarnedAmount >= dailyEarningLimit)
            {
                return 0;
            }
            var totalEarned = await this.GetAmountOfCompletedWork(username);
            if(totalEarned>=lifeTimeLimit)
                return 0;
            return dailyEarningLimit - todaysEarnedAmount;

        }

        public async Task<bool> DeleteRejectedWork(string workId)
        {
            var result = await _repo.DeleteRejectedWork(workId);
            if (result)
            {
                var deleteResult = await _repo.SaveAllAsync();
                if (deleteResult)
                {
                    return true;
                }
            }
            return false;
        }
        public async Task<bool> CheckEligibility(string username)
        {
            var userprofile = await _repo.GetEntityByPrimaryKeyAsync<UserProfile>(username);
            if (userprofile == null)
            {
                return false;
            }
            if (userprofile.NoOfTeamMembers < MINVERIFIERS)
            {
                return false;
            }
            return true;



        }
    }
}
