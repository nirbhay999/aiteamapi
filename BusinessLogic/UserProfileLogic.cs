﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AITeamAPI.DTO;
using AITeamAPI.Entities;
using AITeamAPI.Repository;
using System.IO;
using System.Diagnostics;

namespace AITeamAPI.BusinessLogic
{
    public class UserProfileLogic : IUserProfileLogic
    {
        private IDbRepository _dbRepo;
        private IConfiguration _config;

        public UserProfileLogic(IDbRepository dbRepo, IConfiguration config)
        {
            _dbRepo = dbRepo;
            _config=config;
        }
        public async Task<UserProfile> SaveNewUserProfile(UserProfileDTO userProfileDTO, bool isPaidMember, string currentLoggedInUsername)
        {
            var userProfileExists = await _dbRepo.GetEntityByPrimaryKeyAsync<UserProfile>(currentLoggedInUsername);
            if (userProfileExists != null)
            {
                return null;
            }
            // get the userprofile of verifierTeamId and update the NoOfteamMembers
            var ownerUserProfile = await _dbRepo.GetUserProfileByTeamID(userProfileDTO.VerifierTeamId);
            if (ownerUserProfile != null)
            {
                ownerUserProfile.NoOfTeamMembers = ownerUserProfile.NoOfTeamMembers + 1;
                _dbRepo.Update<UserProfile>(ownerUserProfile);
            }
            else
            {
                return null;
            }

            UserProfile userProfile = new UserProfile();
            Random rnd = new Random();
            userProfile.EmailId = userProfileDTO.EmailId;
            userProfile.Name = userProfileDTO.Name;
            userProfile.DOB = userProfileDTO.DOB;
            userProfile.VerifierTeamId = userProfileDTO.VerifierTeamId;
            userProfile.Pincode = userProfileDTO.Pincode;
            userProfile.IsPaidMember = isPaidMember;
            userProfile.UserName = currentLoggedInUsername;
            userProfile.NoOfTeamMembers = 0;
            userProfile.OwnTeamId = GenerateTeamId(8, rnd);
            string fileName = Guid.NewGuid().ToString() + ".jpg";
            if (userProfileDTO.ProfileImage != null)
            {
                try
                {
                    userProfile.ProfileImage = fileName;
                    File.WriteAllBytes(@".\wwwroot\images\ProfilePictures\" + fileName, Convert.FromBase64String(userProfileDTO.ProfileImage.Split(',')[1]));
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.ToString());
                }
            }

            
            var newlyCreatedProfile = await _dbRepo.Add<UserProfile>(userProfile);
            if (await _dbRepo.SaveAllAsync())
            {
                return newlyCreatedProfile;
            }
                return null;

        }

        public async Task<UserProfile> UpdateUserProfile(UserProfileDTO userProfile,string userName)
        {
            var profile = await _dbRepo.GetEntityByPrimaryKeyAsync<UserProfile>(userName);
            profile.Name = userProfile.Name;
            profile.Pincode = userProfile.Pincode;
            if (userProfile.ProfileImage != null && userProfile.ProfileImage.Contains("base64"))
            {
                string fileName = Guid.NewGuid().ToString() + ".jpg";
                try
                {
                    profile.ProfileImage = fileName;
                    File.WriteAllBytes(@".\wwwroot\images\ProfilePictures\" + fileName, Convert.FromBase64String(userProfile.ProfileImage.Split(',')[1]));
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.ToString());
                }
            }
            var x = _dbRepo.Update<UserProfile>(profile);
            if(await _dbRepo.SaveAllAsync())
            {
                return x;
            }
            return null;

        }

        public async Task<bool> ChangeToPaidMember(string userName)
        {
            var currentUserProfile = await _dbRepo.GetEntityByPrimaryKeyAsync<UserProfile>(userName);
            currentUserProfile.IsPaidMember = true;
            var x = _dbRepo.Update<UserProfile>(currentUserProfile);
            var y = await _dbRepo.SaveAllAsync();
            if (y)
            {
                return true;
            }
            return false;
        }
        private static string GenerateTeamId(int length, Random random)
        {
            string characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            StringBuilder result = new StringBuilder(length);
            for (int i = 0; i < length; i++)
            {
                result.Append(characters[random.Next(characters.Length)]);
            }
            return result.ToString();
        }
    }
}
