﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AITeamAPI.Repository;
using AITeamAPI.Entities;
using Microsoft.Extensions.Configuration;
using System.Security.Cryptography;
using System.Text;

namespace AITeamAPI.BusinessLogic
{
    public class UserFinanceLogic : IUserFinanceLogic
    {
        private static byte[] key = new byte[8] { 34, 97, 46, 193, 233, 168, 219, 175 };
        private IDbRepository _repo;
        private IConfiguration _config;

        public UserFinanceLogic( IDbRepository repo, IConfiguration config)
        {
            _repo = repo;
            _config = config;
        }
        public async Task<bool> CreateEncashmentRequest(int amount, string username)
        {
            try
            {
                var encashmentReq = new EncashmentRequest(amount, username);
                await _repo.Add<EncashmentRequest>(encashmentReq);
                // mark all the verification completed work as paymentreleased
                await _repo.MarkWorkAsPaid(username);
                await _repo.SaveAllAsync();
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }

        }

        public async Task<int> GetEarnedVerfiedAmount(string username)
        {
            return await _repo.GetEarnedAndVerfiedAmount(username);
        }

        public async Task<int> GetEarnedNotVerifiedAmount(string username)
        {
            return await _repo.GetEarnedAndNotVerfiedAmount(username);
        }

        public async Task<Tuple<int, int,string>> GetFinalFeeAfterDiscount (string username, string discountCode)
        {
            var joiningFee = _config.GetValue<int>("JoiningFee");
            var discountPercentage = 0;
            var finalFee = joiningFee;
            var discount = await _repo.GetDiscountCode(username, discountCode);
            if (discount != null)
            {
                if (discount.ValidTill.Subtract(DateTime.UtcNow) > TimeSpan.Zero)
                {
                    discountPercentage = discount.DiscountPercentage;
                    finalFee = joiningFee - (int) Math.Floor((joiningFee * discountPercentage) / 100D);
                }
                
            }

            var encrypted = Crypt(finalFee.ToString(), GetIV(username)); 
            return new Tuple<int, int, string>(discountPercentage, finalFee,encrypted);
        }

        private static byte[] GetIV(string username)
        {
            var x = username.ToCharArray().Select(s => Convert.ToByte(s)).ToList();
            return x.Skip(2).Take(8).ToArray();
        }
       
        public static string Crypt(string text, byte[] iv)
        {
            try
            {
                SymmetricAlgorithm algorithm = DES.Create();
                ICryptoTransform transform = algorithm.CreateEncryptor(key, iv);
                byte[] inputbuffer = Encoding.Unicode.GetBytes(text);
                byte[] outputBuffer = transform.TransformFinalBlock(inputbuffer, 0, inputbuffer.Length);
                return Convert.ToBase64String(outputBuffer);
            }
            catch(Exception ex)
            {
                var exc = ex.ToString();
                return null;
            }
        }

        public string Decrypt(string text, string username)
        {
            var iv = GetIV(username);
            SymmetricAlgorithm algorithm = DES.Create();
            ICryptoTransform transform = algorithm.CreateDecryptor(key, iv);
            byte[] inputbuffer = Convert.FromBase64String(text);
            byte[] outputBuffer = transform.TransformFinalBlock(inputbuffer, 0, inputbuffer.Length);
            return Encoding.Unicode.GetString(outputBuffer);
        }

    }
}
