﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AITeamAPI.DTO;
using AITeamAPI.Entities;

namespace AITeamAPI.BusinessLogic
{
    public interface IUserProfileLogic
    {
        Task<UserProfile> SaveNewUserProfile(UserProfileDTO userProfileDTO, bool isPaidMember, string currentLoggedInUsername);
        Task<UserProfile> UpdateUserProfile(UserProfileDTO userProfileDTO, string currentLoggedInUsername);
        Task<bool> ChangeToPaidMember(string userName);
    }
}
