﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AITeamAPI.Entities;
using AITeamAPI.DTO;
using AITeamAPI.Models;

namespace AITeamAPI.BusinessLogic
{
    public interface IWorkLogic
    {
        Task<List<Work>[]> GetWork(int noOfVerifiers);
        Task<bool> SaveWork(Work work, string username);
        Task<List<VerificationWorkParts>> GetVerificationWorkPartsByUsername(string username);
        Task<List<VerificationWorkParts>> GetVerificationWorkPartsByWorkId(string workId, string username);
        Task<bool> MarkWorkAsVerfied(string workId, string username);
        Task<bool> SaveVideoResponse(VideoResponses res);
        Task<int> GetEarnableAmount(string username, int dailyEarningLimit, int lifeTimeLimit);
        Task<bool> CheckEligibility(string username);
        Task<VideoResponses> GetVideoResponse(string username, string videoId);

        Task<bool> DeleteRejectedWork(string workId);
    }
}
