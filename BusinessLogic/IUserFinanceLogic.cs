﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AITeamAPI.BusinessLogic
{
    public interface IUserFinanceLogic
    {
        Task<bool> CreateEncashmentRequest(int amount, string username);
        Task<Tuple<int, int, string>> GetFinalFeeAfterDiscount(string username, string discountCode);
        string Decrypt(string text, string username);
    }
}
