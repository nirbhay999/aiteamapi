﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using AITeamAPI.Repository;
using AITeamAPI.BusinessLogic;
using System.Collections.Generic;
using Paytm.Checksum;
using AITeamAPI.Entities;
using System;
using Microsoft.Extensions.Configuration;

namespace AITeamAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class UserFinanceController:Controller
    {
        private IUserFinanceLogic _financeLogic;
        private IUserProfileLogic _profileLogic;
        private IConfiguration _config;
        private IDbRepository _repo;

        public UserFinanceController(IDbRepository repo, IUserFinanceLogic financeLogic, IUserProfileLogic profileLogic, IConfiguration config)
        {
            _repo = repo;
            _financeLogic = financeLogic;
            _profileLogic = profileLogic;
            _config = config;
        }
        // Get encashmnet requests

        [HttpGet("GetEncashmentRequests")]
        public async Task<IActionResult> GetEncashmentRequests()
        {
           var encashmentRequets =  await _repo.GetEncashmentRequestsByUsername(HttpContext.User.Identity.Name);
            return Ok(encashmentRequets);
        }

        [HttpGet("UnverifiedWorkPayments")]
        public async Task<IActionResult> GetUnverifiedWorkPayments()
        {

           return Ok(await _repo.GetEarnedAndNotVerfiedAmount(HttpContext.User.Identity.Name));
        }

        [HttpGet("GetCompletedWorks")]
        public async Task<IActionResult> GetCompletedWorks()
        {

            return Ok(await _repo.GetCompletedWorks(HttpContext.User.Identity.Name));
        }

        [HttpGet("VerifiedWorkPayments")]
        public async Task<IActionResult> GetVerifiedWorkPayments()
        {
            return Ok(await _repo.GetEarnedAndNotVerfiedAmount(HttpContext.User.Identity.Name));
        }

        // Get total earned but not yet encashed
        // Create encashment request
        [HttpGet("CreateEncashmentRequest")]
        public async Task<IActionResult> CreateEncashmentRequest()
        {
            var amount = await _repo.GetEarnedAndVerfiedAmount(HttpContext.User.Identity.Name);
            var result = await _financeLogic.CreateEncashmentRequest(amount, HttpContext.User.Identity.Name);
            if (result)
            {
                return Ok("Encashment request created");
            }
            return BadRequest("Exception occured");
        }

        [HttpGet("OpenPayment/{encryptedAmount?}")]
        public ContentResult InitPayTM(string encryptedAmount=null)
        {
            string txnAmount;
            if (encryptedAmount != null)
            {
                try
                {
                    txnAmount = _financeLogic.Decrypt(encryptedAmount, HttpContext.User.Identity.Name);
                }
                catch (Exception ex)
                {
                    return new ContentResult
                    {
                        ContentType = "text/html",
                        StatusCode = BadRequest().StatusCode,
                        Content = ""
                    };
                }
            }
            else
            {
                txnAmount = _config.GetValue<string>("JoiningFee");
            }
            var config = _config.GetSection("PayTM");
            var random = new Random();
            Dictionary<string, string> paytmParams = new Dictionary<string, string>();
            string orderId = HttpContext.User.Identity.Name + "-" + random.Next(0, 1000).ToString();
            paytmParams.Add("CUST_ID", HttpContext.User.Identity.Name);
            paytmParams.Add("ORDER_ID", orderId);
            paytmParams.Add("TXN_AMOUNT", txnAmount);
            paytmParams.Add("MID", config["MerchantMid"]);
            paytmParams.Add("CHANNEL_ID", "WAP");
            paytmParams.Add("WEBSITE", config["WebSite"]);
            paytmParams.Add("CALLBACK_URL", config["CallbackUrl"]);
            paytmParams.Add("INDUSTRY_TYPE_ID", config["IndustryType"]);
            string transactionURL = config["StagingUrl"];
            try
            {
                string paytmChecksum = CheckSum.GenerateCheckSum(config["MerchantKey"], paytmParams);

                string outputHTML = "<center style='padding-left:10px'><h5>Redirecting to paytm...</h5></center>";
                outputHTML += "<center style='padding-left:10px'><h5>Do not refresh or go back!</h5></center>";
                outputHTML += "<form method='post' action='" + transactionURL + "' name='f1' id='f2'>";
                foreach (string key in paytmParams.Keys)
                {
                    outputHTML += "<input type='hidden' name='" + key + "' value='" + paytmParams[key] + "'>";
                }
                outputHTML += "<input type='hidden' name='CHECKSUMHASH' value='" + paytmChecksum + "'>";
                outputHTML += "</form>";
                return new ContentResult
                {
                    ContentType = "text/html",
                    StatusCode = Ok().StatusCode,
                    Content = outputHTML
                };
            }
            catch (Exception ex)
            {
                return new ContentResult
                {
                    ContentType = "text/html",
                    StatusCode = BadRequest().StatusCode,
                    Content = ""
                };
            }
        }

        [AllowAnonymous]
        [HttpPost("paytmcallback")]
        public async Task<IActionResult> Paytmcallback()
        {

            Dictionary<string, string> paytmParams = new Dictionary<string, string>();
            var paytmChecksum = "";
            foreach (string key in Request.Form.Keys)
            {
                paytmParams.Add(key.Trim(), Request.Form[key].ToString().Trim());
            }
            if (paytmParams.ContainsKey("CHECKSUMHASH"))
            {
                paytmChecksum = paytmParams["CHECKSUMHASH"];
                paytmParams.Remove("CHECKSUMHASH");
            }
            bool isValidChecksum = CheckSum.VerifyCheckSum(_config["PayTM:MerchantKey"], paytmParams, paytmChecksum);
            if (isValidChecksum)
            {
                if (paytmParams["RESPCODE"] == "01")
                {
                    //Change user profile to paid member
                    // Get the username from orderId;
                    var username = paytmParams["ORDERID"].Split('-')[0];
                    var result  = await _profileLogic.ChangeToPaidMember(username);
                    if (result)
                    {

                    }
                    else
                    {
                        // payment done but profile not changed to paid
                    }

                }

            }
            return Redirect(Url.Content(_config["PayTM:RedirectUrl"]) + $"#paymentresult?respcode={paytmParams["RESPCODE"]}");



        }

        [HttpGet("CheckDiscountCode/{discountCode}")]
        public async Task<IActionResult> CheckDiscountCodes(string discountCode)
        {
           var result = await _financeLogic.GetFinalFeeAfterDiscount(HttpContext.User.Identity.Name, discountCode);
            return (Ok(result));
        }


    }
}
