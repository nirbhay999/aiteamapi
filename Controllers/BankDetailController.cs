﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AITeamAPI.Entities;
using AITeamAPI.Repository;

namespace AITeamAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class BankDetailController:Controller
    {
        private IDbRepository _repo;

        public BankDetailController(IDbRepository repo)
        {
            _repo = repo;
        }
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var currentUser = HttpContext.User.Identity.Name;
            var bankDetails = await _repo.GetEntityByPrimaryKeyAsync<BankDetail>(currentUser);
            if (bankDetails != null)
            {
                return Ok(bankDetails);
            }
            return NotFound("No bank details stored for the user");
        }
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] BankDetail bankDetail)
        {
            bankDetail.UserName = HttpContext.User.Identity.Name;
            var result = await _repo.UpdateBankDetails(bankDetail);
            var saveResult = await _repo.SaveAllAsync();
            if (saveResult)
            {
                return Ok(result);
            }
            return BadRequest();
        }
    }
}
