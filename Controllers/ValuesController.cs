﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AITeamAPI.Repository;
using AITeamAPI.Entities;
using AITeamAPI.BusinessLogic;
using AITeamAPI.DTO;

namespace AITeamAPI.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private IDbRepository _repo;
        private IWorkLogic _logic;

        public ValuesController(IDbRepository repo, IWorkLogic logic)
        {
            _repo = repo;
            _logic = logic;
        }
        // GET api/values
        [HttpGet]
        public string Get()
        {
            return "good";
        }

        // GET api/values/5
        //[HttpGet("{id}")]
        //public IEnumerable<Work> Get(int id)
        //{
        //    return _repo.GetWork();
        //}

        // POST api/values
        [HttpPost]
        public async Task Post([FromBody]UserProfileDTO value)
        {
            //_repo.Add<UserFinance>(value).Wait();
            //await _repo.SaveAllAsync();
            //await _logic.UpdateUserProfile(value, "9028665804");
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
