﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AITeamAPI.BusinessLogic;
using AITeamAPI.DTO;
using AITeamAPI.Entities;
using AITeamAPI.Services;
using System.IO;
namespace AITeamAPI.Controllers
{
    [Route("api/[controller]")]
    public class AuthController:Controller
    {
        private IConfiguration _config;
        private IMessageSender _sender;
        private SignInManager<AppUser> _signInManager;
        private UserManager<AppUser> _userManager;

        public AuthController(IConfiguration config, UserManager<AppUser> userManager,IMessageSender sender, SignInManager<AppUser> signInManager)
        {
            _config = config;
            _userManager = userManager;
            _sender = sender;
            _signInManager = signInManager;
        }

        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody] LoginDTO loginData)
        {
            try
            {
                var loginResult = await _signInManager.PasswordSignInAsync(loginData.MobileNumber, loginData.Password, loginData.RememberMe, false);
                if (loginResult.Succeeded)
                {
                    return Ok("Login successfull");
                }
            }
            catch(Exception ex)
            {
                return BadRequest(ex.ToString());
            }

            return BadRequest("Login failed. Incorrect ID or Password");


        }
        [HttpPost("Register")]
        public async Task<IActionResult> Register([FromBody] string MobileNumber)
        {
            if (IsDigitsOnly(MobileNumber.Trim()) && MobileNumber.Length == 10) {
                var existingUser = await _userManager.FindByNameAsync(MobileNumber);

                //could not complete registration process
                if(existingUser!=null)
                {
                    var passwordCompareResult = await _userManager.CheckPasswordAsync(existingUser, _config["DefaultPassword"]);
                    if (passwordCompareResult)
                    {
                        await SendOTP(existingUser, "Your OTP for registration is");
                        return Ok("OTP sent");
                    }else
                    {
                        return BadRequest("Mobile Number already registered");
                    }
                }
                var user = new AppUser { UserName = MobileNumber, PhoneNumber = MobileNumber };
                var result = await _userManager.CreateAsync(user, _config["DefaultPassword"]);
                if (result.Succeeded)
                {
                    await SendOTP(user,"Your OTP for registration is");
                    return Ok("OTP sent");

                }
                if (result.Errors.Count<IdentityError>() > 0)
                {
                    var errors = new JsonResult(result.Errors);
                    return BadRequest(errors);
                }
                return BadRequest("Error while registering ");
            }
            return BadRequest("Not a valid mobile number");
        }

        [HttpPost("VerifyOTP")]
        public async Task<IActionResult> VerifyOTP([FromBody] string MobileNumberAndOTP)
        {
            if (MobileNumberAndOTP.Trim().Contains("."))
            {
                string MobileNumber = MobileNumberAndOTP.Split('.')[0];
                string OTP = MobileNumberAndOTP.Split('.')[1];
                if ((MobileNumber.Length == 10) && IsDigitsOnly(MobileNumber) && IsDigitsOnly(OTP)) {
                    var user = await _userManager.FindByNameAsync(MobileNumber);
                    if (user == null)
                    {
                        return BadRequest("Mobile number is not registered");
                    }
                    var result = await _userManager.VerifyChangePhoneNumberTokenAsync(user, OTP, MobileNumber);
                    if (result == true)
                    {
                        user.PhoneNumberConfirmed = true;
                        await _userManager.UpdateAsync(user);
                        return Ok("OTP Verified");
                    }
                }
            }
            return BadRequest("Invalid OTP");
        }


        // Set password during registration process after OTP is verified
        [HttpPost("SetPassword")]
        public async Task<IActionResult> SetPassword([FromBody] SetPassword MobileNumberAndPassword)
        {

                string MobileNumber = MobileNumberAndPassword.MobileNumber;
                string Password = MobileNumberAndPassword.Password;
                var user = await _userManager.FindByNameAsync(MobileNumber);
                {
                    var result = await _userManager.ChangePasswordAsync(user, _config["DefaultPassword"], Password);
                    if (result.Succeeded)
                    {
                        await _signInManager.SignInAsync(user, false);
                        return Ok();
                    }

                }

            return BadRequest("bad password");
        }
        [HttpPost("GetPasswordResetOTP")]
        public async Task<IActionResult> GetPasswordResetOTP([FromBody] string MobileNumber)
        {
            var user = await _userManager.FindByNameAsync(MobileNumber);
            if(user!=null && user.PhoneNumberConfirmed == true)
            {
                var passwordCompareResult = await _userManager.CheckPasswordAsync(user, _config["DefaultPassword"]);
                if (!passwordCompareResult)
                {
                    await SendOTP(user,"Your OTP to reset the password is");
                    return Ok("Password reset OTP sent to registered mobile number");


                }
            }
            return BadRequest("Error changing password");
        }

        [HttpPost("ResetPassword")]
        public async Task<IActionResult> ResetPassword([FromBody] PasswordResetDTO model)
        {
            var user = await _userManager.FindByNameAsync(model.MobileNumber);
            var result = await _userManager.VerifyChangePhoneNumberTokenAsync(user, model.OTP, user.PhoneNumber);
            if (result)
            {
                var removePasswordResult = await _userManager.RemovePasswordAsync(user);
                var passWordChangeResult = await _userManager.AddPasswordAsync(user, model.Password);
                return Ok("Password changed succesfully");
            }
            return BadRequest("Error Occured");
        }

        [HttpPost("Logout")]
        public async Task<IActionResult> Logout()
        {
            if (User.Identity.IsAuthenticated)
            {
                await _signInManager.SignOutAsync();
            }

            // set the expiry date of cookies to negative so that they are deleted
            foreach (var key in HttpContext.Request.Cookies.Keys)
            {
                HttpContext.Response.Cookies.Append(key, "", new CookieOptions() { Expires = DateTime.Now.AddDays(-1) });
            }
            return Ok("Logged out succesfully");
        }

        private bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }
        private async Task SendOTP(AppUser user, string message)
        {
            var OTP = await _userManager.GenerateChangePhoneNumberTokenAsync(user, user.PhoneNumber);
            System.IO.File.WriteAllText(@"c:\test\otp.txt", OTP);
           //_sender.SendSmsAsync(user.PhoneNumber, message + " " + OTP); // to be used in production
        }

        private async Task<bool> VerifyOTP(AppUser user, string OTP)
        {
            return await _userManager.VerifyChangePhoneNumberTokenAsync(user, OTP, user.PhoneNumber);
        }
    }
}
