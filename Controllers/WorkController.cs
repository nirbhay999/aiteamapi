﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AITeamAPI.BusinessLogic;
using AITeamAPI.DTO;
using AITeamAPI.Entities;
using AITeamAPI.Repository;

namespace AITeamAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class WorkController : Controller
    {
        private IWorkLogic workLogic;
        private IDbRepository _repo;

        public WorkController(IWorkLogic _workLogic, IDbRepository repo)
        {
            workLogic = _workLogic;
            _repo = repo;

        }

        [HttpPost("SaveWork")]
        public async Task<IActionResult> SaveWork([FromBody]Work work)
        {
            try
            {
                var result = await workLogic.SaveWork(work, HttpContext.User.Identity.Name);
                if (result)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpPost("SaveVideoResponse")]
        public async Task<IActionResult> SaveVideoResponse([FromBody]VideoResponses res)
        {
            if (!string.IsNullOrEmpty(res.Username) || string.IsNullOrEmpty(res.VideoId) || string.IsNullOrEmpty(res.Actions) || string.IsNullOrEmpty(res.Objects) || string.IsNullOrEmpty(res.Description))
            {
                return BadRequest();
            }
            res.Username = HttpContext.User.Identity.Name;
            var result = await workLogic.SaveVideoResponse(res);
            if (result)
            {
                return Ok();
            }
            return BadRequest();
        }

        [HttpGet("GetWork")]
        public async Task<IActionResult> GetAvaiableWork()
        {
            try
            {
                var username = HttpContext.User.Identity.Name;
                var userProfile = await _repo.GetEntityByPrimaryKeyAsync<UserProfile>(username);
                if (userProfile==null)
                {
                    return BadRequest("Need to have userprofile");
                }
                var worklist = await workLogic.GetWork(userProfile.NoOfTeamMembers);
                var returnList = new List<Work>();
                foreach(var item in worklist)
                {
                    returnList.AddRange(item);
                }
                return Ok(returnList);
            }
            catch(Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpGet("GetVerificationWorks")]
        public async Task<IActionResult> GetVerificationWorkPartsByUsername()
        {
            try
            {
                return Ok(await workLogic.GetVerificationWorkPartsByUsername(HttpContext.User.Identity.Name));
            }
            catch (Exception)
            {

                return BadRequest();
            }
        }

        [HttpPost("GetVideoResponse")]
        public async Task<IActionResult> GetVideoResponse([FromBody] GetVideoResponseDTO data)
        {
            try
            {
                return Ok(await workLogic.GetVideoResponse(data.Username, data.VideoId));
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPost("VerifyWork")]
        public async Task<IActionResult> VerifyWork([FromBody] string workId)
        {
            try
            {

                var result = await workLogic.MarkWorkAsVerfied(workId, HttpContext.User.Identity.Name);
                if (result)
                {
                    return Ok();
                }
                return BadRequest();
            }
            catch (Exception)
            {

                return BadRequest();
            }
        }
        [HttpPost("RejectWork")]
        public async Task<IActionResult> RejectWork([FromBody]string workId)
        {
            try
            {
                var result = await workLogic.DeleteRejectedWork(workId);
                if (result)
                {
                    return Ok();
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
}
