﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using AITeamAPI.DTO;
using AITeamAPI.Entities;
using AITeamAPI.Repository;
using AITeamAPI.BusinessLogic;
namespace AITeamAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class UserProfileController : ControllerBase
    {
        private IUserProfileLogic _profileLogic;
        private IDbRepository _repo;
        public UserProfileController(IDbRepository repo, IUserProfileLogic profileLogic)
        {
            _repo = repo;
            _profileLogic = profileLogic;
        }

        //retrieves a specific userprofile
        [HttpGet]
        public async Task<IActionResult> GetUserProfile()
        {
            var currentUser = HttpContext.User.Identity.Name;
            var result = await _repo.GetEntityByPrimaryKeyAsync<UserProfile>(currentUser);
            if (result != null)
            {
                return Ok(result);
            }
            return NotFound("User Profile not found");
        }

        //Creates a new userprofile
        [HttpPost]
        public async Task<IActionResult> CreateNewProfile([FromBody] UserProfileDTO userProfile)
        {
            try
            {
                var currentUser = HttpContext.User.Identity.Name;
                var x = await _profileLogic.SaveNewUserProfile(userProfile, false, currentUser);
                return Ok(x);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpGet("GetTrainer/{teamId}")]
        public async Task<IActionResult> GetTrainerProfile(string teamId)
        {
            try
            {
                var userProfile = await _repo.GetUserProfileByTeamID(teamId);
                if (userProfile != null)
                {
                    return Ok(new { name = userProfile.Name });
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }


        }
        //Updates existing userprofile
        [HttpPut]
        public async Task<IActionResult> UpdateProfile([FromBody] UserProfileDTO userProfile)
        {
            var currentUser = HttpContext.User.Identity.Name;
            var x = await _profileLogic.UpdateUserProfile(userProfile, currentUser);
            return Ok(x);
        }
        [HttpGet("getpaidstatus")]
        public async Task<IActionResult> IsUserProfilePaid()
        {
            var currentUser = HttpContext.User.Identity.Name;
            var result = await _repo.GetEntityByPrimaryKeyAsync<UserProfile>(currentUser);
            if (result != null)
            {
                return Ok(result.IsPaidMember);
            }
            return NotFound("User Profile not found");
        }
       
       
    }
}
