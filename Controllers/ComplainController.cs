﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using AITeamAPI.Repository;
using AITeamAPI.Entities;
using AITeamAPI.Models;
using System;

namespace AITeamAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ComplainController:Controller
    {
        private IDbRepository _repo;
        public ComplainController(IDbRepository repo)
        {
            this._repo = repo;
        }

        [HttpPost("CreateComplain")]
        public async Task<IActionResult> CreateComplain([FromBody]UserComplain complain)
        {
            try
            {
                UserComplain comp = new UserComplain(HttpContext.User.Identity.Name, complain.Category, complain.Description);

                var x = await _repo.Add<UserComplain>(comp);
                if (complain != null)
                {
                    var result = await _repo.SaveAllAsync();
                    if (result)
                    {
                        return Ok("Complain created");
                    }

                }
            }
            catch
            {
                return BadRequest();
            }
            return BadRequest();
        }
        [HttpGet("GetUserComplains")]
        public async Task<IActionResult> GetUserComplains()
        {
            try
            {
                var username = HttpContext.User.Identity.Name;
                return Ok(await _repo.GetUserComplains(username));
            }
            catch(Exception ex)
            {
                return BadRequest();
            }

        }
        [HttpPost("ResolveComplain")]
        [Authorize(Roles ="Admin")]
        public async Task<IActionResult> ResolveComplain([FromBody]UserComplain complain)
        {
            try
            {
                complain.Status = ComplainStatus.Resolved;
                _repo.Update<UserComplain>(complain);
                await _repo.SaveAllAsync();
                return Ok("Complain Updated");
            }
            catch
            {
                return BadRequest();
            }
        }
        [HttpPost("ReopenComplain")]
        public async Task<IActionResult> ReopenComplain([FromBody]UserComplain complain)
        {
            try
            {
                complain.Username = HttpContext.User.Identity.Name;
                complain.Status =  ComplainStatus.ReOpened;
                _repo.Update<UserComplain>(complain);
                await _repo.SaveAllAsync();
                return Ok("complain reopened");
            }
            catch
            {
                return BadRequest();
            }
        }
        [HttpGet("GetComplains")]
        public async Task<IActionResult> GetComplains([FromBody] ComplainStatus status)
        {
            try
            {
                return Ok(await _repo.GetComplains(status));
            }
            catch
            {
                return BadRequest();
            }
        }


    }
}
