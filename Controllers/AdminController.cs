﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AITeamAPI.Controllers
{
    [Authorize(Roles ="Admin")]
    public class AdminController:Controller
    {

        public async Task<IActionResult> GetOpenTickets()
        {
            return Ok();
        }

        public async Task<IActionResult> GetFinances()
        {
            return Ok();
        }
        public async Task<IActionResult> GetEncashmentRequest()
        {
            return Ok();
        }
        public async Task<IActionResult> InitiateBankTransfer()
        {
            return Ok();
        }
    }
}
