﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AITeamAPI.DTO;
using AITeamAPI.Entities;
using AITeamAPI.Models;
using AITeamAPI.Services;

namespace AITeamAPI.Repository
{
    public class DbRepo : IDbRepository
    {
        private MyDbContext _context;
        public DbRepo(MyDbContext context)
        {
            _context = context;
           _context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public async Task<T> Add<T>(T Entity) where T : class
        {
           var x= await _context.AddAsync(Entity);
           return x.Entity;



        }

        public T Update<T>(T Entity) where T : class
        {
            var x = _context.Update<T>(Entity);
            return x.Entity;
        }
        public void Delete<T>(T Entity) where T : class
        {
            _context.Remove(Entity);
        }
        public async Task<T> GetEntityByPrimaryKeyAsync<T>(string username) where T : class
        {
            return await _context.FindAsync<T>(username);
        }
        public List<UserProfile> GetAllUserProfiles()
        {
            return _context.UserProfiles.ToList();

        }

        public async Task<List<Work>> GetWork(int verifiers)
        {
            //return _context.Works.Where(x => x.MinVerifiers == verifiers && x.Started==false).OrderBy(x=> Guid.NewGuid()).Take(10).ToList();
            return _context.Works.FromSql($"SELECT * FROM works WHERE WORKS.minverifiers = {verifiers} ORDER BY RAND() LIMIT 12").ToList();
        }
       

        public List<Video> GetVideos(int maxLength, int numberOfVideos)
        {
            return _context.Videos.FromSql("SELECT * FROM VIDEOS WHERE VIDEOS.LENGTH <= " + maxLength.ToString() + " ORDER BY RAND() LIMIT " + numberOfVideos.ToString()).ToList();
        }

        public async Task<bool> SaveAllAsync()
        {
            try
            {
                var saveResult = await _context.SaveChangesAsync();
                if(saveResult > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public async Task<int> UpdateUserProfile(UserProfileDTO userProfile,string username)
        {
            var uf = _context.UserProfiles.Find(username);
            uf.Name = userProfile.Name;
            _context.Entry<UserProfile>(uf).State = EntityState.Modified;
            return await _context.SaveChangesAsync();
        }

        public async Task<BankDetail> UpdateBankDetails(BankDetail bankDetail)
        {
            var existing = _context.BankDetails.Find(bankDetail.UserName);
            if (existing==null)
            {
              var result = await this.Add(bankDetail);
              return result;
            }
            else
            {
                var result = this.Update(bankDetail);
                return result;
            }
        }

        public async Task<UserProfile> GetUserProfileByTeamID(string teamId)
        {
            try
            {
                var result = await _context.UserProfiles.SingleOrDefaultAsync(x => x.OwnTeamId == teamId);
                return result;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public async Task<List<UserProfile>> GetTeamMembers(string ownerTeamId)
        {
            return await _context.UserProfiles.Where(x => x.VerifierTeamId == ownerTeamId).ToListAsync();
        }

        public Task<UserProfile> GetUserProfile(string username)
        {
            throw new NotImplementedException();
        }


        public async Task<List<EncashmentRequest>> GetEncashmentRequestsByUsername(string username)
        {
            return await _context.EncashmentRequests.Where(x => x.UserName == username).ToListAsync();
        }

        public  int GetTotalEncashmentRequetsAmount(string username)
        {
            return  _context.EncashmentRequests.Where(x => x.UserName == username).Sum(x => x.RequestedAmount);
        }

        public async Task<int> GetEarnedAndVerfiedAmount(string username)
        {
           var verfiedWorks =await _context.VerificationWorks.Where(x => x.Username == username && x.Status == WorkStatus.VerficationCompleted).ToListAsync();
           return verfiedWorks.Sum(x => x.Payments);

        }

        public async Task<int> GetEarnedAndNotVerfiedAmount(string username)
        {
            var verfiedWorks = await _context.VerificationWorks.Where(x => x.Username == username && x.Status == WorkStatus.PendingVerification).ToListAsync();
            return verfiedWorks.Sum(x => x.Payments);
        }



        public async Task<int> GetTodaysCompletedWorkAmount(string username)
        {
            var verfiedWorks = await _context.VerificationWorks.Where(x => x.Username == username).ToListAsync();
            return verfiedWorks.Where(x => x.CompletionDate == DateTime.Today).Sum(x => x.Payments);
        }

        public async Task<List<UserProfile>> GetVerifiers(string username, bool? isPaid = null)
        {
            var userProfile = await this.GetEntityByPrimaryKeyAsync<UserProfile>(username);
            if (isPaid == null)
            {
                return await _context.UserProfiles.Where(x => x.VerifierTeamId == userProfile.OwnTeamId).ToListAsync();
            }

            return await _context.UserProfiles.Where(x => x.VerifierTeamId == userProfile.OwnTeamId && x.IsPaidMember == isPaid).ToListAsync();

        }

        public async Task<List<VerificationWorkParts>> GetVerificationWorkPartsByUsername(string username)
        {
            return await _context.VerificationWorkParts.Where(x => x.Username == username && x.VerificationComplete==false).Include(x=>x.VerificationWork).ToListAsync();
        }

        public async Task<List<VerificationWorkParts>> GetVerificationWorkPartsByWork(string workId)
        {
            return await _context.VerificationWorkParts.Where(x => x.VerificationWorkId == workId).ToListAsync();
        }

        public async Task<VideoResponses> GetVideoResponse(string username, string videoId)
        {
            return await _context.VideoResponses.Where(x => x.Username == username && x.VideoId == videoId).FirstOrDefaultAsync();
        }

        public async Task<bool> DeleteRejectedWork(string workId)
        {
            try
            {
                var work = await _context.VerificationWorks.Where(y => y.Id == workId).FirstOrDefaultAsync();
                var workParts = await _context.VerificationWorkParts.Where(x => x.VerificationWorkId == workId).ToListAsync();

                _context.VerificationWorkParts.RemoveRange(workParts);
                _context.VerificationWorks.Remove(work);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> MarkWorkAsPaid(string username)
        {
            try
            {
                var verifiedWorks = await _context.VerificationWorks.Where(x => x.Status == WorkStatus.VerficationCompleted && x.Username == username).ToListAsync();
                if (verifiedWorks.Count == 0)
                {
                    return false;
                }
                foreach (var work in verifiedWorks)
                {
                    work.Status = WorkStatus.PaymentReleased;
                }
                _context.VerificationWorks.UpdateRange(verifiedWorks);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
        public async Task<List<VerificationWork>> GetCompletedWorks(string username)
        {
            return await _context.VerificationWorks.Where(x => x.Username == username).ToListAsync();
        }

        public async Task<List<UserComplain>> GetComplains(ComplainStatus status)
        {
            return await _context.UserComplains.Where(x => x.Status == status).ToListAsync();
        }

        public async Task<List<UserComplain>> GetUserComplains(string username)
        {
            return await _context.UserComplains.Where(x => x.Username == username).ToListAsync();
        }

        public async Task<DiscountCode> GetDiscountCode(string username, string discountCode)
        {
            return await _context.DiscountCode.FirstOrDefaultAsync(x => x.UserName == username && x.Code == discountCode);
        }
    }
}
