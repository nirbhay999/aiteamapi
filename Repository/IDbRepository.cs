﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AITeamAPI.DTO;
using AITeamAPI.Entities;
using AITeamAPI.Models;

namespace AITeamAPI.Repository
{
    public interface IDbRepository
    {
        List<Video> GetVideos(int maxLength, int numberOfVideos);
        List<UserProfile> GetAllUserProfiles();
        Task<T> Add<T>(T Entity) where T : class;
        void Delete<T>(T Entity) where T : class;
        T Update<T>(T Entity) where T : class;
        Task<bool> SaveAllAsync();
        Task<BankDetail> UpdateBankDetails(BankDetail bankDetail);

        Task<UserProfile> GetUserProfile(string username);
        Task<T> GetEntityByPrimaryKeyAsync<T>(string username) where T : class;
        Task<UserProfile> GetUserProfileByTeamID(string ownerTeamId);
        Task<List<UserProfile>> GetTeamMembers(string ownerTeamId);
        Task<List<EncashmentRequest>> GetEncashmentRequestsByUsername(string username);
        int GetTotalEncashmentRequetsAmount(string username);
        Task<int> GetEarnedAndVerfiedAmount(string username);
        Task<int> GetEarnedAndNotVerfiedAmount(string username);
        Task<int> GetTodaysCompletedWorkAmount(string username);

        Task<List<Work>> GetWork(int verifiers);

        //Verification works Methods
        Task<List<UserProfile>> GetVerifiers(string username, bool? isPaid = null);
        Task<List<VerificationWorkParts>> GetVerificationWorkPartsByUsername(string username);
        Task<List<VerificationWorkParts>> GetVerificationWorkPartsByWork(string workId);
        Task<VideoResponses> GetVideoResponse(string username, string videoId);
        Task<bool> DeleteRejectedWork(string workId);
        Task<bool> MarkWorkAsPaid(string username);

        Task<List<VerificationWork>> GetCompletedWorks(string username);

        Task<List<UserComplain>> GetComplains(ComplainStatus status);

        Task<List<UserComplain>> GetUserComplains(string username);

        // Discount Codes
        Task<DiscountCode> GetDiscountCode(string username, string discountCode);
    }
}
