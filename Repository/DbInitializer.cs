﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AITeamAPI.Entities;
using Microsoft.AspNetCore.Identity;
using System.Text;

namespace AITeamAPI.Repository
{
    public class DbInitializer
    {
        private MyDbContext _context;
        private UserManager<AppUser> _userMgr;
        private string[] strArry = new string[] { "DPAa-arZkcA",
"lyPBzUSKBLU",
"sKDSnceMzIw",
"Zv4NnN2g7tk",
"uaxsUMoqkvA",
"OIS4RezGsQ0",
"UOV_9HtEjCE",
"S41ovMZ03x8",
"D3NGXtoY",
"XBdr_HeTu8I",
"IbXvd3epSQQ",
"YikemsjJo-o",
"YPTpCFVzEjI",
"PF5u3L3wSxk",
"5Y83RhiTO2M",
"toA8WmjYrw",
"_YiGymGO5VY",
"VK0HUGI8CsE",
"y6tBrMYb8Z0",
"4EDiw41TdII",
"BxyXk1sr2io",
"i17faW7AM24",
"B2iS33aBwjg",
"twS5t5595EA",
"uWYRtm6kYw8",
"Qs56GW6ggVw",
"69dWB29Jo_g",
"wpV_4Ox722M",
"rZa7mocZeng",
"qEwBM8t0e88",
"w6sLIfbmCp0",
"JGTqeZxHqZ4",
"b4Mal13maAk",
"dtIlOd_bWAE",
"SxPz6_-FxxY",
"6h2KVOtYLqk",
"O4N4ttIv8P4",
"4x_w_gUBS8I",
"4etXTeiWi9k",
"rF_VANiEY18",
"eJz-tTuakP4",
"uXZhC6KqHYw",
"baeqaYiEC5I",
"12uiqMfVnYY",
"ekhV3onTbZQ",
"oroutkPMs1w",
"hmf-HgAFytA",
"Va2DxgERRug",
"f1kh3UrmPC4",
"cFugK8V_Qqs",
"qvfBQ1gUMY0",
"Bti4g8PU9oE",
"hBUbSxRwqkY",
"Nr2c4XjejjY",
"87yAQ9wAUig",
"6TA2uHrHEjc",
"KV2jjvrSIIU",
"lJsI0gUmbJc",
"gjlaH6oSMCU",
"IB5p-G3aIOo",
"QTXL71B9xn4",
"g4pJhAjQDjg",
"8VSeMQpvKGM",
"yXw-h7yWmZQ" };


        public DbInitializer(MyDbContext context, UserManager<AppUser> usermgr)
        {
            _context = context;
            _userMgr = usermgr;
        }

        public void Seed()
        {
            AppUser user = new AppUser()
            {
                UserName = "9028665804",
                Email = "test@test.com",
                PhoneNumber = "9028665804"
            };
            AppUser user1 = new AppUser()
            {
                UserName = "9999999999",
                Email = "t@t.com",
                PhoneNumber = "9028665804"
            };

            var videos = new List<Video>();
            var rand = new Random();
            for (int i = 0; i < 64; i++)
            {
                var video = new Video();
                video.Length = rand.Next(30, 200);
                video.VideoId = this.strArry[i];
                videos.Add(video);
            }

            //_userMgr.CreateAsync(user, "Patna@1234").Wait();
            //_userMgr.CreateAsync(user1, "Patna@1234").Wait();
            //_context.AddRange(GetUserProfile());
            //_context.AddRange(GetBankDetail());
            //_context.AddRange(GetEncashmentReq());
            //_context.AddRange(GetUserFinance());
            _context.AddRange(GetWork());
            //_context.AddRange(videos);
            _context.SaveChanges();


        }
        private static string GetRandomString(int length, Random random)
        {
            string characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            StringBuilder result = new StringBuilder(length);
            for (int i = 0; i < length; i++)
            {
                result.Append(characters[random.Next(characters.Length)]);
            }
            return result.ToString();
        }
        private IEnumerable<UserProfile> GetUserProfile()
        {
            List<UserProfile> list = new List<UserProfile>
            {
                new UserProfile()
                {
                    UserName="9028665804",
                    Name="Nirbhay",
                    EmailId="Pune"

                }
            };
            return list;
        }

        private IEnumerable<BankDetail> GetBankDetail()
        {
            List<BankDetail> list = new List<BankDetail>
            {
                new BankDetail()
                {
                    UserName="9028665804",
                    IFSCCode="123424234",
                    AccountNumber="121313213213",
                }
        };
            return list;
        }

        private IEnumerable<UserFinance> GetUserFinance()
        {
            List<UserFinance> list = new List<UserFinance>
            {
                new UserFinance("9028665804")
                {
                    LifeTimeEarningLimit=1000,
                    TotalEarned=200,
                    TotalEncashed=100,
                    WIPCash=100
                }

            };

            return list;
        }

        private IEnumerable<EncashmentRequest> GetEncashmentReq()
        {
            List<EncashmentRequest> list = new List<EncashmentRequest>
            {
                new EncashmentRequest(100,"9028665804"),
                new EncashmentRequest(200,"9028665804"),
                new EncashmentRequest(300,"9028665804"),
            };
            return list;
        }

        private IEnumerable<Work> GetWork()
        {
            var rand = new Random();
            List<Work> list = new List<Work>();
            for(int i = 1001; i < 2001; i++)
            {
                var x = new Work()
                {
                  
                    Payment = 600,
                    MinVerifiers = 3,
                    Started=false,
                    Completed=false,
                    
                };
                var noOfVideos = rand.Next(7, 20);
                var listStr = new List<string>();
                for(int j = 0; j < noOfVideos; j++)
                {
                    listStr.Add(strArry[rand.Next(64)]);
                }
                x.VideosStr = string.Join(';', listStr);
                x.TotalTimeInSeconds = rand.Next(120, 500);
                list.Add(x);
            }
            for (int i = 2001; i < 3001; i++)
            {
                var x = new Work()
                {
                   
                    Payment = 700,
                    MinVerifiers = 4,
                    Started=false,
                    Completed=false
                };
                var noOfVideos = rand.Next(7, 20);
                var listStr = new List<string>();
                for (int j = 0; j < noOfVideos; j++)
                {
                    listStr.Add(strArry[rand.Next(64)]);
                }
                x.VideosStr = string.Join(';', listStr);
                x.TotalTimeInSeconds = rand.Next(120, 500);
                list.Add(x);
            }
            for (int i = 3001; i < 4001; i++)
            {
                var x = new Work()
                {
                   
                    Payment = 800,
                    MinVerifiers = 6,
                    Started=false,
                    Completed=false                    
                };
                var noOfVideos = rand.Next(7, 20);
                var listStr = new List<string>();
                for (int j = 0; j < noOfVideos; j++)
                {
                    listStr.Add(strArry[rand.Next(64)]);
                }
                x.VideosStr = string.Join(';', listStr);
                x.TotalTimeInSeconds = rand.Next(120, 500);
                list.Add(x);
            }
            for (int i = 4001; i < 5001; i++)
            {
                var x = new Work()
                {
                   
                    Payment = 900,
                    MinVerifiers = 7,
                    Started=false,
                    Completed=false
                    
                };
                var noOfVideos = rand.Next(7, 20);
                var listStr = new List<string>();
                for (int j = 0; j < noOfVideos; j++)
                {
                    listStr.Add(strArry[rand.Next(64)]);
                }
                x.VideosStr = string.Join(';', listStr);
                x.TotalTimeInSeconds = rand.Next(120, 500);
                list.Add(x);
            }
            for (int i = 5001; i < 6001; i++)
            {
                var x = new Work()
                {
                    Payment = 1000,
                    MinVerifiers = 8,
                    Started=false,
                    Completed=false
                    
                };
                var noOfVideos = rand.Next(7, 20);
                var listStr = new List<string>();
                for (int j = 0; j < noOfVideos; j++)
                {
                    listStr.Add(strArry[rand.Next(64)]);
                }
                x.VideosStr = string.Join(';', listStr);
                x.TotalTimeInSeconds = rand.Next(120, 500);
                list.Add(x);
            }
            for (int i = 6001; i < 7001; i++)
            {
                var x = new Work()
                {
                  
                    Payment = 1100,
                    MinVerifiers = 9,
                    Completed=false,
                    Started=false
                    
                };
                var noOfVideos = rand.Next(7, 20);
                var listStr = new List<string>();
                for (int j = 0; j < noOfVideos; j++)
                {
                    listStr.Add(strArry[rand.Next(64)]);
                }
                x.VideosStr = string.Join(';', listStr);
                x.TotalTimeInSeconds = rand.Next(120, 500);
                list.Add(x);
            }
            for (int i = 7001; i < 8001; i++)
            {
                var x = new Work()
                {
                   
                    Payment = 1200,
                    MinVerifiers = 10,
                    Started=false,
                    Completed=false
                    
                };
                var noOfVideos = rand.Next(7, 20);
                var listStr = new List<string>();
                for (int j = 0; j < noOfVideos; j++)
                {
                    listStr.Add(strArry[rand.Next(64)]);
                }
                x.VideosStr = string.Join(';', listStr);
                x.TotalTimeInSeconds = rand.Next(120, 500);
                list.Add(x);
            }

            return list;
        }



    }
}

