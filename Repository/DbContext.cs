﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AITeamAPI.Entities;

namespace AITeamAPI.Repository
{
    public class MyDbContext : IdentityDbContext
    {
        private IConfiguration _config;
        public MyDbContext(DbContextOptions options, IConfiguration config)
            : base(options)
        {
            _config = config;
        }
        //DB Sets
        public DbSet<AppUser> AppUsers { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<BankDetail> BankDetails { get; set; }
        public DbSet<Video> Videos { get; set; }
        public DbSet<VideoResponses> VideoResponses { get; set; }
        public DbSet<EncashmentRequest> EncashmentRequests { get; set; }
        public DbSet<VerificationWork> VerificationWorks { get; set; }
        public DbSet<VerificationWorkParts> VerificationWorkParts { get; set; }
        public DbSet<UserComplain> UserComplains { get; set; }
        public DbSet<DiscountCode> DiscountCode { get; set; }
        public DbSet<Work> Works { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<AppUser>();
            modelBuilder.Entity<Video>();
            modelBuilder.Entity<UserComplain>();
            modelBuilder.Entity<VideoResponses>();//.HasOne<Video>().WithMany(x => x.Responses).HasForeignKey("VideoId");
            modelBuilder.Entity<UserProfile>()
                .HasOne(s => s.ApplicationUser)
                .WithOne(s => s.UserProfile)
                .HasForeignKey<UserProfile>("UserName")
                .HasPrincipalKey<AppUser>("UserName");

            modelBuilder.Entity<UserProfile>().HasAlternateKey(a => a.OwnTeamId);
            modelBuilder.Entity<BankDetail>()
                .HasOne(s => s.ApplicationUser)
                .WithOne(s => s.BankDetail)
                .HasForeignKey<BankDetail>("UserName")
                .HasPrincipalKey<AppUser>("UserName");
            modelBuilder.Entity<VerificationWork>()
                .HasMany(s => s.Verfiers)
                .WithOne(s => s.VerificationWork)
                .HasForeignKey(s => s.VerificationWorkId);
            modelBuilder.Entity<EncashmentRequest>()
                .HasOne(s=>s.ApplicationUser)
                .WithMany(x=>x.EncashmentRequests)
                .HasForeignKey("UserName")
                .HasPrincipalKey("UserName");
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)

        {
            base.OnConfiguring(optionsBuilder);
        }
    }
}
