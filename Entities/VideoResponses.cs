﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AITeamAPI.Entities
{
    public class VideoResponses
    {
        [Key]
        public string Id { get; set; }
        [Required(AllowEmptyStrings =false)]
        public string VideoId { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Objects { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Actions { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Description { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Username { get; set; }
        public virtual Video Video { get; set; }
    }
}
