﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace AITeamAPI.Entities
{
    public class BankDetail
    {
        [Key]
        public string UserName { get; set; }
        [Required]
        public string IFSCCode { get; set; }
        [Required]
        public string AccountNumber { get; set; }
        [Required]
        public string AccountHolderName { get; set; }
        public AppUser ApplicationUser { get; set; }
    }
}
