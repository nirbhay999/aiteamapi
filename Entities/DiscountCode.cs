﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AITeamAPI.Entities
{
    public class DiscountCode
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public int DiscountPercentage { get; set; }
        public DateTime ValidTill { get; set; }
        public string Code { get; set; }
    }
}
