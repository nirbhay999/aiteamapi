﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AITeamAPI.Models;

namespace AITeamAPI.Entities
{
    public class UserComplain
    {
        public UserComplain()
        {
            // Parameterless constructor for EF
        }
        public UserComplain(string username, ComplaintType category, string description)
        {
            Random rand = new Random();
            this.Id = rand.Next(100000, 10000000).ToString();
            this.ComplainDateTime = DateTime.UtcNow;
            this.Status = ComplainStatus.InProgress;
            this.Description = description;
            this.Category = category;
            this.Username = username;

        }

        public string Id { get; set; }
        public DateTime ComplainDateTime { get; set; }
        public string Username { get; set; }
        public ComplaintType Category { get; set; }
        public string Description { get; set; }
        public ComplainStatus Status { get; set; }
        public List<ComplainResponses> Responses { get; set; }

    }

    public class ComplainResponses
    {
        public string Id { get; set; }
        public DateTime ResponseTime { get; set; }
        public string Response { get; set; }
        public UserComplain Complain { get; set; }
    }
}
