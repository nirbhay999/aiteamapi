﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AITeamAPI.Entities
{
    public class Video
    {
        [Key]
        public string VideoId { get; set; }

        public int Length { get; set; }

        public List<VideoResponses> Responses { get; set; }
    }
}
