﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using AITeamAPI.Models;

namespace AITeamAPI.Entities
{
    public class VerificationWorkParts
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string VerificationWorkId { get; set; }
        public string VideoId { get; set; }
        public bool VerificationComplete { get; set; }
        public DateTime CompletionDate { get; set; }
        public DateTime AssignedDate { get; set; }
        public VerificationWork VerificationWork { get; set; }

    }

    public class VerificationWork
    {
        [Key]
        public string Id { get; set; }
        public List<VerificationWorkParts> Verfiers { get; set; }
        public int Payments { get; set; }
        public int MinVerifiers { get; set;}
        public WorkStatus Status { get; set; }
        public string Username { get; set; }
        public DateTime? CompletionDate { get; set; }

    }


}
