﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AITeamAPI.Entities
{
    public class Work
    {
        [Key]
        public int Id { get; set; }
        public int Payment { get; set; }
        public int MinVerifiers { get; set; }
        public string VideosStr { get; set; }
        public int TotalTimeInSeconds { get; set; }
        public bool Started { get; set; }
        public bool Completed { get; set; }
    }
}
