﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AITeamAPI.Entities
{
    public class UserFinance
    {
        public UserFinance(string Username)
        {
            this.LifeTimeEarningLimit = 0;
            this.TotalEarned = 0;
            this.TotalEncashed = 0;
            this.WIPCash = 0;
            this.UserName = Username;
        }
        [Key]
        public string UserName { get; set; }

        [Required]
        public int LifeTimeEarningLimit { get; set; }

        // contains amount of all the work done till now , does not include wip cash
        public int TotalEarned { get; set; }

        // contains amount for which the actual credit from bank is done
        public int TotalEncashed { get; set; }

        // Includes earning which the user will get from works which are pending verification or are with the user waiting completion
        public int WIPCash { get; set; }

        public AppUser ApplicationUser { get; set; }

    }
}
