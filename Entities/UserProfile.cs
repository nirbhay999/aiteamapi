﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AITeamAPI.Entities
{
    public class UserProfile
    {
        public UserProfile()
        {
            this.ProfileCreationDate = new DateTime().ToUniversalTime();
        }
        [Key]
        public string UserName { get; set; }
        [Required]
        public string Name { get; set; }
        public string EmailId { get; set; }
        public string Pincode { get; set; }
        public string OwnTeamId { get; set; }
        public string VerifierTeamId { get; set; }
        public DateTime DOB { get; set; }
        public bool IsPaidMember { get; set; }
        public int NoOfTeamMembers { get; set; }
        public string ProfileImage { get; set; }
        public DateTime ProfileCreationDate { get; set; }
        public AppUser ApplicationUser { get; set; }
    }
}
