﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AITeamAPI.Entities
{
    public class EncashmentRequest
    {
        public EncashmentRequest()
        {

        }
        public EncashmentRequest(int amount,string userName)
        {
            this.EncashmentRequestDate = DateTime.UtcNow.AddHours(5.5).Date;
            this.TransferredToUserAccount = false;
            this.ActualCreditedDate = null;
            this.RequestedAmount = amount;
            this.UserName = userName;
            this.BankTransRef = string.Empty;
        }
        public string Id { get; set; }
        public DateTime EncashmentRequestDate { get; set; }
        public Boolean TransferredToUserAccount { get; set; }
        public DateTime? ActualCreditedDate { get; set; }

        public int RequestedAmount { get; set; }

        public string UserName { get; set; }

        public string BankTransRef { get; set; }

        public AppUser ApplicationUser { get; set; }
    }
}
