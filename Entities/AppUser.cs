﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace AITeamAPI.Entities
{
    public class AppUser:IdentityUser
    {
        public string Holder { get; set; }
        public UserProfile UserProfile { get; set; }
        [NotMapped]
        public BankDetail BankDetail { get; set; }
        [NotMapped]
        public UserFinance UserFinance { get; set; }
        [NotMapped]
        public List<EncashmentRequest> EncashmentRequests { get; set; }
        [NotMapped]
        public List<VerificationWork> VerificationWorks { get; set; }
    }
}
